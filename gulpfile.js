var gulp        = require('gulp');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;
var sass        = require('gulp-sass');
var watch       = require('gulp-livereload');

// browser-sync task for starting the server.
gulp.task('browser-sync', function() {
    //watch files
    var files = [
    './style.css',
    './*.php'
    ];

    //initialize browsersync
    browserSync.init(files, {
    //browsersync with a php server
    proxy: "vins-koehly.site",
    notify: false
    });
});

// Sass task, will run when any SCSS files change & BrowserSync
// will auto-update browsers
gulp.task('sass', function () {
    return gulp.src('sass/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./'))
        .pipe(reload({stream:true}));
});

// Watch sass files
gulp.task('default', ['sass', 'browser-sync'], function () {
    gulp.watch("sass/**/*.scss", ['sass']);
    gulp.watch("organisms/**/*.scss", ['sass']);
    gulp.watch("index/**/*.scss", ['sass']);
    gulp.watch("chi-siamo/**/*.scss", ['sass']);
    gulp.watch("vini/**/*.scss", ['sass']);
    gulp.watch("contatti/**/*.scss", ['sass']);
    gulp.watch("carrello/**/*.scss", ['sass']);
    gulp.watch("chi-siamo/**/*.scss", ['sass']);
    gulp.watch("chi-siamo/*.scss", ['sass']);
    gulp.watch("legal-information/**/*.scss", ['sass']);
});

// Watch php files
gulp.task('watch', function(){
    livereload.listen();
    gulp.watch('organisms/**/*.php', livereload.reload);
    gulp.watch('index/**/*.php', livereload.reload);
    gulp.watch('chi-siamo/**/*.php', livereload.reload);
    gulp.watch('vini/**/*.php', livereload.reload);
    gulp.watch('contatti/**/*.php', livereload.reload);
    gulp.watch('index.php', livereload.reload);
});
