<?php
/* SCRIPT PHP NECESSAIREMENT EN TETE DE TOUTES LES PAGES WEB.
  le session_start() doit impérativement être la première instruction PHP en haut de page.
*/
session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title>Formulaire de contact</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>

    <script>
        // à mettre dans le fichier main.js
        $(document).ready(function () {

            // on cache la div #sucess
            $('#success').hide();
            // on cache la div #load
            $('#load').hide();

            $('#contact').submit(function () {
                var valid = true;
                // message erreur nom
                if ($('#nome').val() == '') {
                    $('#nome').prev('.error').fadeIn(1000).text('Indicare il Suo nome');
                    valid = false;
                } else {
                    $('#nome').prev('.error').slideUp();
                }
                // message erreur prénom
                if ($('#cognome').val() == '') {
                    $('#cognome').prev('.error').fadeIn(1000).text('Indicare il Suo cognome');
                    valid = false;
                } else {
                    $('#cognome').prev('.error').slideUp();
                }
                // message erreur rue
                if ($('#via').val() == '') {
                    $('#via').prev('.error').fadeIn(1000).text('Indicare la Sua via');
                    valid = false;
                } else {
                    $('#via').prev('.error').slideUp();
                }
                // message erreur code postal
                if ($('#cap').val() == '') {
                    $('#cap').prev('.error').fadeIn(1000).text('Indicare il Suo CAP');
                    valid = false;
                } else {
                    $('#cap').prev('.error').slideUp();
                }
                // message erreur ville
                if ($('#citta').val() == '') {
                    $('#citta').prev('.error').fadeIn(1000).text('Indicare la Sua città');
                    valid = false;
                } else {
                    $('#citta').prev('.error').slideUp();
                }

                // message erreur téléphone
                if ($('#telefono').val() == '') {
                    $('#telefono').prev('.error').fadeIn(1000).text('Indicare il Suo telefono');
                    valid = false;
                } else {
                    $('#telefono').prev('.error').slideUp();
                }
                // message erreur email
                if (!$('#email').val().match(/^[a-z0-9._-]+@[a-z0-9._-]+\.[a-z]{2,6}$/)) {
                    $('#email').prev('.error').fadeIn(1000).text('Indicare un\'email valido');
                    valid = false;
                } else {
                    $('#email').prev('.error').slideUp();
                }
                // message erreur message
                if ($('#messaggio').val() == '') {
                    $('#messaggio').prev('.error').fadeIn(1000).text('Indicare il Suo messaggio');
                    valid = false;
                } else {
                    $('#messaggio').prev('.error').slideUp();
                }

                if (valid == true) {
                    // AJAX
                    // Déclaration des variable correspondant aux champs du formulaire
                    var nome = $('#nome').val();
                    var cognome = $('#cognome').val();
                    var via = $('#via').val();
                    var cap = $('#cap').val();
                    var citta = $('#citta').val();
                    var telefono = $('#telefono').val();
                    var email = $('#email').val();
                    var messaggio = $('#messaggio').val();
                    var dataString = 'nome=' + nome + '&cognome=' + cognome + '&via=' + via + '&cap=' + cap + '&citta=' + citta + '&telefono=' + telefono + '&email=' + email + '&messaggio=' + messaggio;

                    // fonction de jquery qui permet de faire de l'ajax
                    $.ajax({
                        type: 'POST',
                        url: 'functions/contactform.php',
                        data: dataString,

                        beforeSend: function () {
                            $('#load').fadeIn();
                        },
                        success: function (req) {
                            $('#load').fadeOut();
                            $('#contact').slideUp();
                            $('#success').slideDown().text('Messaggio inviato.');
                            console.log(req);
                        }
                    });
                }
                return false;
            });
        });
    </script>
    <style>
        input[type="text"], label, textarea { display:block; padding-top: 5px; padding-bottom:5px;}
        label[for="newsletter"] {display:inline !important;}
    </style>
</head>

<body>


    <div>
        <h1>Contattici</h1>
        <h2>Indirizzo</h2>
        <ul>
            <li>Koehly</li>
            <li>64, Rue Général de Gaulle</li>
            <li>67600 Kintzheim - Alsazia</li>
            <li>Francia</li>
        </ul>
        <p>Mail: vins@koehly.fr</p>
        <p>Telefono: 0033 03 88 82 70 49</p>
        <!-- Google Maps -->
        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d10625.947462342116!2d7.390520865051262!3d48.25500450810597!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8918472594a15b44!2sKoehly+Jean-Marie+%26+Joseph!5e0!3m2!1sfr!2sfr!4v1478339594613" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        <h2>Formulario di contatto:</h2>
        <div id="success">
            <!-- va contenir le message d'envoi réussi -->

        </div>
        <form id="contact" method="post" action="functions/contactform.php">
            <div>
                <label for="nome">Nome</label>
                <!-- message d'erreur en cas de champ non valide -->
                <span class="error" style="color:red"></span>
                <input id="nome" name="nome" type="text" placeholder="Nome" value="<?php if(isset($nome)) echo $nome; ?>">
            </div>
            <div>
                <label for="cognome">Cognome</label>
                <!-- message d'erreur en cas de champ non valide -->
                <span class="error" style="color:red"></span>
                <input id="cognome" name="cognome" type="text" placeholder="Cognome" value="<?php if(isset($cognome)) echo $cognome; ?>">
            </div>
            <div>
                <label for="telefono">Numero di telefono</label>
                <!-- message d'erreur en cas de champ non valide -->
                <span class="error" style="color:red"></span>
                <input id="telefono" name="telefono" type="text" placeholder="Numero di telefono" value="<?php if(isset($telefono)) echo $telefono; ?>">
            </div>
            <div>
                <label for="email">Email</label>
                <!-- message d'erreur en cas de champ non valide -->
                <span class="error" style="color:red"></span>
                <input id="email" name="email" type="text" placeholder="Indirizzo email" value="<?php if(isset($email)) echo $email; ?>">
            </div>
            <div>
                <h3>Indirizzo</h3>
                <div>

                    <label for="via">Via</label>
                    <!-- message d'erreur en cas de champ non valide -->
                    <span class="error" style="color:red"></span>
                    <input id="via" name="via" type="text" placeholder="Via" value="<?php if(isset($via)) echo $via; ?>">
                </div>
                <div>
                    <label for="cap">CAP</label>
                    <!-- message d'erreur en cas de champ non valide -->
                    <span class="error" style="color:red"></span>
                    <input id="cap" name="cap" type="text" placeholder="CAP" value="<?php if(isset($cap)) echo $cap; ?>">
                </div>
                <div>
                    <label for="citta">Città</label>
                    <!-- message d'erreur en cas de champ non valide -->
                    <span class="error" style="color:red"></span>
                    <input id="citta" name="citta" type="text" placeholder="Città" value="<?php if(isset($citta)) echo $citta; ?>">
                </div>
            </div>
            <div>
                <label for="messaggio">Richista informazioni</label>
                <!-- message d'erreur en cas de champ non valide -->
                <span class="error" style="color:red"></span>
                <textarea id="messaggio" name="messaggio" rows="5"><?php if(isset($messaggio)) echo $messaggio; ?></textarea>
            </div>
            <div>
                <input id="newsletter" name="newsletter" type="checkbox" value="<?php if(isset($newsletter)) echo $newsletter; ?>">
                <label for="newsletter">Desidero ricevere la newsletter</label>
            </div>
            <div>
                <input name="inviare" type="submit" value="invia">
            </div>
        </form>
        <div id="load">
            <!-- div contenant le loader -->Envoi en cours…</div>
    </div>
</body>

</html>
