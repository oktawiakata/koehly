<?php
/*
  11 FONCTIONS ACTUELLEMENT :
  - produits()
  - produit($id)
  - categories()
  - clients()
  - nombre_clients()
  - commandes()
  - connexion()
  - deconnexion()
  - ajouter()
  - modifier()
  - supprimer()
  
*/
// Affiche la liste des produits
function produits(){
    global $bdd;
    $requete ="SELECT * FROM vins INNER JOIN categorie ON vins.id_cat = categorie.id_cat ORDER BY ref_vin ASC";
    $produits = $bdd->query($requete);
    $produits = $produits->fetchAll();
    return $produits;

}
// Affiche le produit sélectionné (id)
function produit($id) {
    global $bdd;

    $id = (int)$_GET["id"];

    $produit = $bdd->prepare("SELECT * FROM vins INNER JOIN categorie ON vins.id_cat = categorie.id_cat WHERE ref_vin = ?");
    $produit->execute([$id]);
    $produit = $produit->fetch();

    if(empty($produit))
        header("Location: liste.php");
    else
        return $produit;
}
// Affiche les catégories
function categories(){
    global $bdd;
    $requete ="SELECT * FROM categorie ORDER BY id_cat ASC";
    $categories = $bdd->query($requete);
    $categories = $categories->fetchAll();
    return $categories;

}
// Fonction pour afficher la liste des clients
function clients(){
  global $bdd;
  $req = "SELECT * FROM client WHERE statut = 0";
  $clients = $bdd->prepare($req);
  $clients->execute();
  $clients = $clients->fetchAll();
  return $clients;
}
// Affiche le nombre de clients
function nombre_clients(){
  global $bdd;
  $req = "SELECT COUNT(*) FROM client WHERE statut = 0";
  $nombreclients = $bdd->prepare($req);
  $nombreclients->execute();
  $nombreclients = $nombreclients->fetch()[0];
  return $nombreclients;
}
// affiche les commandes (pas les détails des commandes)
function commandes(){
  global $bdd;
  $req = "SELECT * FROM commande";
  $commmandes = $bdd->prepare($req);
  $commmandes->execute();
  $commmandes = $commmandes->fetchAll();
  return $commmandes;
}
// Se connecter à l'espace admin en définissant une session spéciale admin
function connexion(){
    global $bdd;
    extract($_POST);
    $erreur = "Les identifiants sont erronés. ";

    $connexion = $bdd->prepare("SELECT id_client, mail_client, mdp_client, statut FROM client WHERE mail_client = ?");
    $connexion->execute([$email]);
    $connexion=$connexion->fetch();

    if(password_verify($_POST["password"], $connexion["mdp_client"]) && $connexion["statut"]==1){
      $_SESSION['admin'] = $connexion["id_client"];
      header("Location: ../admin/dashboard.php");
    } else {
      return $erreur;
    }
}
// Se déconnecter
function deconnexion(){
    unset($_SESSION["admin"]);
    session_destroy();
    header("Location:../admin/index.php");
}
// Ajouter un produit au catalogue
function ajouter(){
    // code pour ajouter un produit
    global $bdd;
    extract($_POST);

    // données du formulaire sous forme de variable (commodité)

    $lib = strip_tags($_POST["libelle"]);
    $prix = strip_tags($_POST["prix"]);
    $cat = strip_tags($_POST["categorie"]);
    $zone = strip_tags($_POST["zoneprod"]);
    $raisin = strip_tags($_POST["typeraisin"]);
    $recolte = strip_tags($_POST["recolteraisin"]);
    $deg = strip_tags($_POST["degrevin"]);
    $aff = strip_tags($_POST["affinage"]);
    $temp = strip_tags($_POST["temperature"]);
    $pte = strip_tags($_POST["proprietes"]);
    $contenance = strip_tags($_POST["contenance"]);
    $mill = strip_tags($_POST["millesime"]);
    $degustation = strip_tags($_POST["degustation"]);
    $mariage = strip_tags($_POST["mariage"]);
    $qte = strip_tags($_POST["qtedispo"]);
    $image = basename($_FILES["file"]["name"]);
    // variable pour récupérer le chemin de l'image
    // fonction basename() qui permet de ne récupérer uniquement le nom de l'image
    $alt = strip_tags($_POST["altattribute"]);

    $validation = true;

    $erreurs = []; // tableau vide qui contiendra les erreurs

    // ajouter pour la photo.
    // Fonction upload de fichier
    if(!isset($_FILES["file"]) OR $_FILES["file"]["error"] > 0){
      $validation = false;
      $erreurs[] = "Indiquer un fichier.";
    }
    if($_FILES["file"]["size"] <= $_POST['MAX_FILE_SIZE'] ){
        $infofichier = pathinfo($image);
        $extension = $infofichier['extension'];
        $extensions_autorisees = array('jpg', 'jpeg', 'png', 'gif');
    }else{
      $erreurs[]="Fichier trop lourd";
    }

    if (!in_array($extension, $extensions_autorisees)){
      $validation = false;
      $erreurs[] = "Format d'image non valide.";

    }

    if(empty($lib)
    || empty($prix)
    || empty($cat)
    || empty($zone)
    || empty($raisin)
    || empty($recolte)
    || empty($deg)
    || empty($aff)
    || empty($temp)
    || empty($pte)
    || empty($contenance)
    || empty($mill)
    || empty($degustation)
    || empty($mariage)
    || empty($qte)
    || empty($alt)
  ) {
        $validation = false;
        $erreurs[]= "Tous les champs sont obligatoires";

    }
    if($validation){
      if (in_array($extension, $extensions_autorisees)){
             $uploaddir = '../../img/products/';
             // enregistrement de l'image dans le dossier img/products !
            move_uploaded_file($_FILES["file"]["tmp_name"], $uploaddir.$image);
      }

        $req = 'INSERT INTO vins (libelle_vin, prix_vin, id_cat, zone_prod_vin, raisin_vin, recolte_raisin, affinage_vin, degre_vin, temperature_service, proprietes_vin, contenance_vin, millesime_vin, degustation_vin, mariage_vin, quantite_disponible, photo_vin, photo_alt, date) VALUES (:libelle_vin, :prix_vin, :id_cat, :zone_prod_vin, :raisin_vin, :recolte_raisin, :affinage_vin, :degre_vin, :temperature_service, :proprietes_vin, :contenance_vin, :millesime_vin, :degustation_vin,  :mariage_vin, :quantite_disponible, :photo_vin, :photo_alt, NOW())';
        $nouveauproduit = $bdd->prepare($req);
        $nouveauproduit->execute([
            ":libelle_vin" => htmlentities($lib),
            ":prix_vin" => htmlentities($prix),
            ":id_cat" =>htmlentities($cat),
            ":zone_prod_vin" => htmlentities($zone),
            ":raisin_vin" => htmlentities($raisin),
            ":recolte_raisin" =>htmlentities($recolte),
            ":affinage_vin" => htmlentities($aff),
            ":degre_vin" => htmlentities($deg),
            ":temperature_service" => htmlentities($temp),
            ":proprietes_vin" => htmlentities($pte),
            ":contenance_vin" => htmlentities($contenance),
            ":millesime_vin" => htmlentities($mill),
            ":degustation_vin" => nl2br(htmlentities($degustation)),
            ":mariage_vin" => nl2br(htmlentities($mariage)),
            ":quantite_disponible" => htmlentities($qte),
            ':photo_vin'=>htmlentities($image),
            ":photo_alt" => htmlentities($alt)
        ]);
        $nouveauproduit->closeCursor();

            unset($_POST["libelle"]);
            unset($_POST["prix"]);
            unset($_POST["categorie"]);
            unset($_POST["zoneprod"]);
            unset($_POST["typeraisin"]);
            unset($_POST["recolteraisin"]);
            unset($_POST["affinage"]);
            unset($_POST["degrevin"]);
            unset($_POST["temperature"]);
            unset($_POST["proprietes"]);
            unset($_POST["contenance"]);
            unset($_POST["millesime"]);
            unset($_POST["degustation"]);
            unset($_POST["mariage"]);
            unset($_POST["qtedispo"]);
            unset($_POST["altattribute"]);
            // destruction des variables
            unset($lib);
            unset($prix);
            unset($cat);
            unset($zone);
            unset($raisin);
            unset($recolte);
            unset($deg);
            unset($aff);
            unset($temp);
            unset($pte);
            unset($contenance);
            unset($mill);
            unset($degustation);
            unset($mariage);
            unset($qte);
            unset($alt);
    }
    return $erreurs;
}
// Modifier un produit du catalogue
function modifier(){
    global $bdd;

    extract($_POST);

    $id = (int)$_GET["id"];
    $erreurs = [];

    // données du formulaire sous forme de variable (commodité)

    $lib = strip_tags($_POST["libelle"]);
    $prix = strip_tags($_POST["prix"]);
    $cat = strip_tags($_POST["categorie"]);
    $zone = strip_tags($_POST["zoneprod"]);
    $raisin = strip_tags($_POST["typeraisin"]);
    $recolte = strip_tags($_POST["recolteraisin"]);
    $deg = strip_tags($_POST["degrevin"]);
    $aff = strip_tags($_POST["affinage"]);
    $temp = strip_tags($_POST["temperature"]);
    $pte = strip_tags($_POST["proprietes"]);
    $contenance = strip_tags($_POST["contenance"]);
    $mill = strip_tags($_POST["millesime"]);
    $degustation = strip_tags($_POST["degustation"]);
    $mariage = strip_tags($_POST["mariage"]);
    $qte = strip_tags($_POST["qtedispo"]);
    $alt = strip_tags($_POST["altattribute"]);
    $image = basename($_FILES["file"]["name"]);


    $validation = true;

    // ajouter pour la photo.
    // Fonction upload de fichier
    if(!isset($_FILES["file"]) OR $_FILES["file"]["error"] > 0){
      $validation = false;
      print_r($_FILES["file"]);
      $erreurs[] = "Indiquer un fichier.";
    }
    if($_FILES["file"]["size"] <= $_POST['MAX_FILE_SIZE'] ){
        $infofichier = pathinfo($image);
        $extension = $infofichier['extension'];
        $extensions_autorisees = array('jpg', 'jpeg', 'png', 'gif');
    }else{
      $validation = false;
      $erreurs[]="Fichier trop lourd";
    }

    if (!in_array($extension, $extensions_autorisees)){
      $validation = false;
      $erreurs[] = "Format d'image non valide.";

    }

        if(empty($lib)
        || empty($prix)
        || empty($cat)
        || empty($zone)
        || empty($raisin)
        || empty($recolte)
        || empty($deg)
        || empty($aff)
        || empty($temp)
        || empty($pte)
        || empty($contenance)
        || empty($mill)
        || empty($degustation)
        || empty($mariage)
        || empty($qte)
        || empty($alt)
      ) {
            $validation = false;
            $erreurs[]= "Tous les champs sont obligatoires";

        }
    if($validation) {

      if (in_array($extension, $extensions_autorisees)){
             $uploaddir = '../../img/products/';
             // enregistrement de l'image dans le dossier img/products !
            move_uploaded_file($_FILES["file"]["tmp_name"], $uploaddir.$image);
      }
      $req = 'UPDATE vins SET id_cat = :id_cat, libelle_vin = :libelle_vin,prix_vin = :prix_vin, temperature_service = :temperature_service,contenance_vin = :contenance_vin,degre_vin = :degre_vin,raisin_vin = :raisin_vin,recolte_raisin = :recolte_raisin,affinage_vin = :affinage_vin, millesime_vin = :millesime_vin, zone_prod_vin = :zone_prod_vin, proprietes_vin = :proprietes_vin, mariage_vin = :mariage_vin, degustation_vin = :degustation_vin, quantite_disponible = :quantite_disponible,photo_vin = :photo_vin, photo_alt = :photo_alt WHERE vins.ref_vin = :ref_vin';

      $modifierproduit = $bdd->prepare($req);

      $modifierproduit->bindParam(":id_cat", $cat, PDO::PARAM_INT);
      $modifierproduit->bindParam(":libelle_vin", $lib, PDO::PARAM_STR);
      $modifierproduit->bindParam(":prix_vin", $prix, PDO::PARAM_STR);
      $modifierproduit->bindParam(":temperature_service", $temp, PDO::PARAM_STR);
      $modifierproduit->bindParam(":contenance_vin", $contenance, PDO::PARAM_STR);
      $modifierproduit->bindParam(":degre_vin", $deg,PDO::PARAM_STR);
      $modifierproduit->bindParam(":raisin_vin", $raisin,PDO::PARAM_STR);
      $modifierproduit->bindParam(":recolte_raisin", $recolte,PDO::PARAM_STR);
      $modifierproduit->bindParam(":affinage_vin", $aff,PDO::PARAM_STR);
      $modifierproduit->bindParam(":millesime_vin", $mill,PDO::PARAM_INT);
      $modifierproduit->bindParam(":zone_prod_vin", $zone,PDO::PARAM_STR);
      $modifierproduit->bindParam(":proprietes_vin", $pte,PDO::PARAM_STR);
      $modifierproduit->bindParam(":mariage_vin", $mariage,PDO::PARAM_STR);
      $modifierproduit->bindParam(":degustation_vin", $degustation,PDO::PARAM_STR);
      $modifierproduit->bindParam(":quantite_disponible", $qte,PDO::PARAM_INT);
      $modifierproduit->bindParam(":photo_vin", $image, PDO::PARAM_STR);
      $modifierproduit->bindParam(":photo_alt", $alt,PDO::PARAM_STR);
      $modifierproduit->bindParam(":ref_vin", $id, PDO::PARAM_INT);

      $modifierproduit->execute();
      $modifierproduit->closeCursor();
      // On force le rafraîchissement de la page afin d'afficher les nouvelles informations
      header("Refresh:0");
    }
      return $erreurs;
}
// Supprimer un fichier du catalogue
function supprimer(){
    // fonction pour supprimer un produit
    global $bdd;

    $id = (int)$_GET["id"];

    $uploaddir = '../../img/products/';
    $image = $bdd->prepare('SELECT photo_vin FROM vins WHERE ref_vin = ?'); // nom du fichier
    $image->execute([$id]);
    $image = $image->fetch()["photo_vin"];
    unlink($uploaddir.$image);

    $supprimer = $bdd->prepare("DELETE FROM vins WHERE ref_vin = ?");
    $supprimer->execute([$id]);


}
