<?php
/**
 * Verifie si le panier existe, le creer sinon
 * @return booleen
 */
function creationPanier(){
   if (!isset($_SESSION['panier'])){

      $_SESSION['panier']= array();
      $_SESSION['panier']['idProduit'] = array();
      $_SESSION['panier']['libelleProduit'] = array();
      $_SESSION['panier']['qteProduit'] = array();
      $_SESSION['panier']['prixProduit'] = array();
   }
   return true;
}
/**
 * Ajoute un article dans le panier
 * @return void
 */
function ajouterArticle($idProduit,$libelleProduit,$qteProduit,$prixProduit){
   //Si le panier existe
   if (creationPanier())
   {

      //Si le produit existe deja on ajoute seulement la quantit�
      $positionProduit = array_search($idProduit,  $_SESSION['panier']['idProduit']);
    echo $positionProduit;
      if ($positionProduit !== false)
      {

         $_SESSION['panier']['qteProduit'][$positionProduit] += $qteProduit ;

      }
      else
      {
	    array_push( $_SESSION['panier']['idProduit'],$idProduit);
         array_push( $_SESSION['panier']['libelleProduit'],$libelleProduit);
         array_push( $_SESSION['panier']['qteProduit'],$qteProduit);
         array_push( $_SESSION['panier']['prixProduit'],$prixProduit);

		}
   }
   else
   echo "Un problème est survenu veuillez contacter l'administrateur du site.";
}
function modifierQTeArticle($idProduit,$qteProduit){
   //Si le panier existe
   if (creationPanier())
   {
      //Si la quantit� est positive on modifie sinon on supprime l'article
      if ($qteProduit > 0)
      {
         //Recharche du produit dans le panier
         $positionProduit = array_search($idProduit,  $_SESSION['panier']['idProduit']);

         if ($positionProduit !== false)
         {
            $_SESSION['panier']['qteProduit'][$positionProduit] = $qteProduit ;
         }
      }
      else
      supprimerArticle($idProduit);
   }
   else
   echo "Un problème est survenu veuillez contacter l'administrateur du site.";
}
function supprimerArticle($idProduit){
   //Si le panier existe
   if (creationPanier())
   {
      //Nous allons passer par un panier temporaire
      $tmp=array();
	    $tmp['idProduit'] = array();
      $tmp['libelleProduit'] = array();
      $tmp['qteProduit'] = array();
      $tmp['prixProduit'] = array();
      //$tmp['verrou'] = $_SESSION['panier']['verrou'];

      for($i = 0; $i < count($_SESSION['panier']['idProduit']); $i++)
      {
         if ($_SESSION['panier']['idProduit'][$i] !== $idProduit)
         {
		         array_push( $tmp['idProduit'],$_SESSION['panier']['idProduit'][$i]);
             array_push( $tmp['libelleProduit'],$_SESSION['panier']['libelleProduit'][$i]);
             array_push( $tmp['qteProduit'],$_SESSION['panier']['qteProduit'][$i]);
             array_push( $tmp['prixProduit'],$_SESSION['panier']['prixProduit'][$i]);
         }

      }
      //On remplace le panier en session par notre panier temporaire  jour
      $_SESSION['panier'] =  $tmp;
      //On efface notre panier temporaire
      unset($tmp);
   }
   else
   echo "Un probl�me est survenu veuillez contacter l'administrateur du site.";
}
/**
 * Montant total du panier
 * @return int
 */
function MontantGlobal(){
   $total=0;
   for($i = 0; $i < count($_SESSION['panier']['idProduit']); $i++)
   {
      $total += $_SESSION['panier']['qteProduit'][$i] * $_SESSION['panier']['prixProduit'][$i];
   }
   return $total;
}
/**
 * Fonction de suppression du panier
 * @return void
 */
function supprimePanier(){
   unset($_SESSION['panier']);
}
/**
 * Compte le nombre d'articles diff�rents dans le panier
 * @return int
 */
function compterArticles()
{
   if (isset($_SESSION['panier']))
   return count($_SESSION['panier']['idProduit']);
   else
   return 0;
}
/*
  Cette fonction permet de calculer les frais de livraison
  Tariffe comprensiva di IVA:
  CHIEDERE PREVENTIVO PER SPEDIZIONI SUPERIORI ALLE 30 BOTTIGLIE ATTENZIONE:
  Le tariffe sotto riportate sono standard:
  * da 0 a 10 kg (6 bott.) 25,00 euro
  * da 10 a 20 kg (12 bott.) 25,00 euro
  * da 20 a 30 kg (18 bott.) 40,00 euro
  Simplification / Utiliser les frais de livraison pour les îles comme frais de livraison normaux applicable partout.
  Livraison gratuite à partir de 30 bouteilles.
*/
function fraisLivraison(){
  // Création des variables correspondant à la quantité et au montant de la livraison
  $qte;
  $liv;
  if($qte <= 6){
    $liv = 25;
  }elseif($qte <= 12){
    $liv = 25;
  }elseif($qte < 30){
    $liv = 40;
  }else{
    $liv = 0;
  }
  // on retourne la valeur du montant des frais de livraison
  return $liv;
}
