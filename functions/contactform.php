<?php

/* ------------------- CONTACT FORM ------------------------
-------------- WEBCOQTAIL COMMUNICATION AGENCY--------------
--------------- PROJET TUTEURE 2016 M2 2LCE ----------------
---------- Web developer Back-end : Maral Sabbagh-----------
------------ maralsabbagh@gmail.com for inquiries-----------
-----------------------------------------------------------*/


    require_once 'bdd.php';
    
    $bdd = bdd();
    
    
    $erreurs = []; // tab vide qui contiendra les erreurs
            if(!empty($_POST) && !empty($_POST['nome']) && !empty($_POST['cognome']) && !empty($_POST['telefono']) && !empty($_POST['email']) && !empty($_POST['via']) && !empty($_POST['cap']) && !empty($_POST['citta']) && !empty($_POST['messaggio'])){
                extract($_POST); 
                
                $nom = strip_tags($_POST['cognome']);
                $prenom = strip_tags($_POST['nome']);
                                
                $rue = strip_tags($_POST['via']);
                $cp = strip_tags($_POST['cap']);
                $ville = strip_tags($_POST['citta']);
                
                $mail = strip_tags($_POST['email']);
                $tel = strip_tags($_POST['telefono']);
                $message = strip_tags($_POST['messaggio']);
                
                $newsletter = strip_tags($_POST['newsletter']);
                
                /* ---------------------------------------------------------------------------
                --------- TEST D'INTEGRITE DES CHAMPS DU FORMULAIRE A FAIRE EN PHP -----------
                ---------------------------------------------------------------------------- */
                // Ces tests devront être fait également du côté AJAX
                
                // Nom et prénom doivent être des chaînes de caractères, Nom et prénom ne doivent pas être identiques
                
                if($prenom == $nom) {
                    $erreurs = "Il nome ed il cognome non possono essere gli stessi.";
                    return $erreurs;
                }
                // Rue doit contenir au moins un numéro de rue
                
                // le code postal doit être une suite de caractères composée uniquement de chiffres
                
                // mail -> doit être une adresse email valide filter_var($mail, FILTER_VALIDATE_EMAIL)
                
                // le message doit être au moins de 200 caractères
                if(strlen($message)<200){
                    $erreurs = "Le message doit être au minimum de 200 caractères";
                    return $erreurs;
                }
                // le téléphone doit être une chaîne de caractère composée uniquement de chiffres (sans espace ni de caractères spéciaux)
                
                /* ----------------------------------
                --------- CONNEXION A LA BDD --------
                ------------------------------------*/
                global $bdd;
                
                /* ---------------------------------------------------------------------------
                --------- PREPARATION DE LA REQUETE : INSERTION DANS LA TABLE CONTACT --------
                ---------------------------------------------------------------------------- */
                
                $donnees = 'INSERT INTO contact (prenom, nom, tel, mail, rue, cp, ville, message, newsletter, date) VALUES (:nome, :cognome, :telefono, :email, :via, :cap, :citta, :messaggio, :newsletter, NOW())';
                 
                $req = $bdd->prepare($donnees);
                $req->execute(array(':nome'=>$prenom,':cognome'=>$nom,':telefono'=>$tel,':email'=>$mail,':via'=>$rue,':cap'=>$cp,':citta'=>$ville,':messaggio'=>$message, ':newsletter'=>$newsletter));
                $req->closeCursor();
                 /* ----------------------------------------------------------------------------------------------------------------------
                --------- PREPARATION DE LA REQUETE : INSERTION DANS LA TABLE NEWSLETTER SI LA VALEUR DE NEWSLETTER = 1 cad. TRUE --------
                -------------------------------------------------------------------------------------------------------------------------- */
                if($newsletter = 1){
                   $donnees = 'INSERT INTO newsletter (email) VALUES (:email)';
                   $req = $bdd->prepare($donnees);
                   $req->execute(array(":email"=>$mail));
                   $req->closeCursor();
                } 
                /* ----------------------------------
                --------- SCRIPT ENVOI MAIL ---------
                ------------------------------------ */
                $to ='webcoqtail@gmail.com';
                $sujet ='Nouveau message de '.$nom;
                $message ='
                        <h1>Nouveau message de '.$nom.' '.$prenom.' </h1>
                        <h2>Adresse e-mail: '.$mail.'</h2>
                        <p>'. nl2br($message) . '</p>
                        ';
                $headers='From: '.$nom. '<' .$mail. '>'."\r\n";
                // Pour envoyer un mail HTML, l'en-tête Content-type doit être défini
                $headers .= 'MIME-Version: 1.0' . "\r\n";
                $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";

                mail($to, $sujet, $message, $headers);

                 /* ------------------------------------------------------------------------------------------------------------------
                --------- VIDER LES CHAMPS DU FORMULAIRE POUR EVITER LES DOUBLONS D'INSERTION DANS LA BASE EN CAS DE REFRESH ---------
                ------------------------------------------------------------------------------------------------------------------- */          unset($prenom);
                unset($nom);
                unset($tel);
                unset($mail);
                unset($message);
                unset($rue);
                unset($cp);
                unset($ville);
                
                
                }else{  
                        extract($_POST); 
                        if(empty($nom)){ $erreur = "<p>Indicare il Suo cognome</p>"; echo $erreur;}
                        if(empty($prenom)){ $erreur = "<p>Indicare il Suo nome</p>"; echo $erreur;}
                        if(empty($tel)){ $erreur = "<p>Indicare il Suo telefono</p>"; echo $erreur;}
                        if(empty($rue)){ $erreur = "<p>Indicare la Sua via</p>"; echo $erreur;}
                        if(empty($cp)){ $erreur = "<p>Indicare il Suo CAP</p>"; echo $erreur;}
                        if(empty($ville)){ $erreur = "<p>Indicare la Sua città</p>"; echo $erreur;}
                        if(empty($mail) || !filter_var($email, FILTER_VALIDATE_EMAIL)){ $erreur = "<p>Indicare un'email valido</p>"; echo $erreur;}
                        if(empty($message)){ $erreur = "<p>Indicare il Suo messaggio</p>"; echo $erreur;}
                        echo('Erreur d\'enregistrement dans la BDD');
                } 

        
