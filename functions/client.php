<?php
/*
  8 FONCTIONS ACTUELLEMENT :
  - existe($email)
  - inscription()
  - connexion()
  - deconnexion()
  - inscription_newsletter()
  - infos_client()
  - infos_commande()
  - modifier_infos_client()
*/
// vérifier si le mail entré existe en bdd
function existe($email) {
    global $bdd;

    $resultat = $bdd->prepare("SELECT COUNT(*) FROM client WHERE mail_client = ?");
    $resultat->execute([$email]);
    $resultat = $resultat->fetch()[0];

    return $resultat;
}
// fonction inscription
function inscription(){
        global $bdd;

        extract($_POST);


        $validation = true;
        $erreur = [];


        $nom = strip_tags($_POST['nom']);
        $prenom = strip_tags($_POST['prenom']);
        $email = strip_tags($_POST['email']);
        $mdp = strip_tags($_POST['password']);
        $mdp_conf = strip_tags($_POST['password-conf']);

        // si tous les champs sont remplis
        if(empty($nom) || empty($prenom) || empty($email)  || empty($mdp) || empty($mdp_conf)) {
            $validation = false;
            $erreur[] = "Tous les champs doivent être remplis";
        }

        // verif du mdp
        if($mdp != $mdp_conf){
            $validation = false;
            $erreur[] = "Les mots de passes ne sont pas identiques";

        }
        if(strlen($mdp)<6 ||  strlen($mdp_conf)<6){
            $validation = false;
            $erreur[] = "Votre mot de passe doit être d'au minimum 6 caractères";

        }

        $tabChiffres = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"];
        $checked = false;

        foreach ($tabChiffres as $car){
            if(strpos($mdp, $car) != false) $checked = true;
            /* if(strpos($mdp, $car) != false) {
                $checked = true;
             }
            */
        }
        if(!$checked){
            $erreur[]="Votre mot de passe doit contenir des caractères alphanumériques";
        }

        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            // filter validate email
            $validation = false;
            $erreur[] = "Veuillez indiquer une adresse email valide";

        }
        if(existe($email) == 1){
          $validation = false;
          $erreur[]="L'adresse mail est déjà utilisée";
        }
        if($validation){
                // Requête
                $mesDonnees = 'INSERT INTO client (nom_client, prenom_client, mail_client,  mdp_client, date) VALUES (:nom_client, :prenom_client, :mail_client, :mdp_client, NOW())';
                $inscription = $bdd->prepare($mesDonnees);
                $inscription->execute(array(
                    ':nom_client'=>htmlentities($nom),
                    ':prenom_client'=>htmlentities($prenom),
                    ':mail_client'=>htmlentities($email),
                    ':mdp_client'=>password_hash($mdp,PASSWORD_DEFAULT)
                ));

                unset($_POST['nom']);
                unset($_POST['prenom']);
                unset($_POST['email']);
                // destruction des variables
                unset($email);
                unset($nom);
                unset($prenom);
                unset($mdp);
                unset($mdp_conf);

        }
    return $erreur;


}
// fonction connexion
function connexion(){
    global $bdd;
    extract($_POST);
    $erreur = "Les identifiants sont erronés. ";

    $connexion = $bdd->prepare("SELECT id_client, mail_client, mdp_client, statut FROM client WHERE mail_client = ?");
    $connexion->execute([$email]);
    $connexion=$connexion->fetch();

    if(password_verify($_POST["password"], $connexion["mdp_client"]) && $connexion["statut"]==0){
      $_SESSION['client'] = $connexion["id_client"];
      header("Location: ../areaclienti/dashboard.php");
    } else {
      return $erreur;
    }
}
// fonction déconnexion
function deconnexion(){
    unset($_SESSION["client"]);
    session_destroy();
    header("Location:../index.php");
}
// fonction inscription newsletter
function inscription_newsletter(){
  global $bdd;
  extract($_POST);

  $email = strip_tags($_POST['mail']);
  $validation = true;
  $erreurs = [];
  if(existe($email) == 1  OR !filter_var($email, FILTER_VALIDATE_EMAIL)){
    $validation = false;
    if(existe($email)== 1){
      $erreurs[] = "L'adresse mail est déjà utilisée";
    }
    if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
      $erreurs[] = "Adresse email invalide";
    }
  }
  if($validation){
    $req = "INSERT INTO newsletter (email, date_inscription) VALUES (:email, NOW())";
    $inscriptionnewsletter = $bdd->prepare($req);
    $inscriptionnewsletter->bindParam(":email", $mail, PDO::PARAM_STR);
    $inscriptionnewsletter->execute();
    $inscriptionnewsletter->closeCursor();

  }
}
// affichage des informations relatives au client
function infos_client(){
  global $bdd;
  $client = $bdd->prepare("SELECT * FROM client WHERE id_client = ?");
  $client->execute([$_SESSION["client"]]);
  $client = $client->fetch();
  return $client;
}
// affichage des informations relatives aux commandes du client
function infos_commande(){
  global $bdd;
  $commandes = $bdd->prepare("SELECT * FROM commande INNER JOIN constituee_de INNER JOIN client ON commande.num_commande = constituee_de.num_com WHERE constituee_de.id_client = client.id_client  AND client.id_client = ?");
  $commandes->execute([$_SESSION["client"]]);
  $commandes = $commandes->fetchAll();
  return $commandes;
}
// fonction pour modifier les informations relatives aux clients
function modifier_infos_client(){
    global $bdd;

    extract($_POST);

    $id = (int)$_SESSION["client"];

    $erreurs = [];

    // données du formulaire sous forme de variables (commodité)

    $nom = strip_tags($_POST["nom"]);
    $prenom = strip_tags($_POST["prenom"]);
    $email = strip_tags($_POST["mail"]);
    $tel = strip_tags($_POST["tel"]);
    $rue = strip_tags($_POST["rue"]);
    $cp = strip_tags($_POST["cp"]);
    $ville = strip_tags($_POST["ville"]);

    // COORDONNEES BANCAIRES
    $tit_cb = strip_tags($_POST["titulaire_cb"]);
    $type_cb = strip_tags($_POST["card"]);
    $num_cb = strip_tags($_POST["numero_cb"]);
    $mois_valid_cb = strip_tags($_POST["mois_cb"]);
    $annee_valid_cb = strip_tags($_POST["annee_cb"]);
    $num_secu_cb = strip_tags($_POST["secu_cb"]);

    // newsletter
    $newsletter = strip_tags($_POST["newsletter"]);

    $validation = true;


    if(!empty($nom)
        AND !empty($prenom)
        AND !empty($email)
        AND !empty($tel)
        AND !empty($rue)
        AND !empty($cp)
        AND !empty($ville)
        AND !empty($tit_cb)
        AND !empty($type_cb)
        AND !empty($num_cb)
        AND !empty($mois_valid_cb)
        AND !empty($annee_valid_cb)
        AND !empty($num_secu_cb)
        AND !empty($newsletter)
      ) {
        $modifier_infos_client = $bdd->prepare("UPDATE client
          SET nom_client = :nom_client,
              prenom_client = :prenom_client,
              mail_client = :mail_client,
              tel_client = :tel_client,
              adresse_client = :adresse_client,
              cp_client = :cp_client,
              ville_client = :ville_client,
              type_cb = :type_cb,
              titulaire_cb = :titulaire_cb,
              num_cb = :num_cb,
              mois_valid = :mois_valid,
              an_valid = :an_valid,
              num_secu = :num_secu,
              newsletter = :newsletter
              WHERE id_client = :id_client");

        $modifier_infos_client->bindParam(":nom_client", $nom, PDO::PARAM_INT);
        $modifier_infos_client->bindParam(":prenom_client", $prenom, PDO::PARAM_STR);
        $modifier_infos_client->bindParam(":mail_client", $email, PDO::PARAM_STR);
        $modifier_infos_client->bindParam(":tel_client", $tel, PDO::PARAM_STR);
        $modifier_infos_client->bindParam(":adresse_client", $rue, PDO::PARAM_STR);
        $modifier_infos_client->bindParam(":cp_client", $cp, PDO::PARAM_STR);
        $modifier_infos_client->bindParam(":ville_client", $ville, PDO::PARAM_STR);
        $modifier_infos_client->bindParam(":type_cb", $type_cb, PDO::PARAM_STR);
        $modifier_infos_client->bindParam(":titulaire_cb", $tit_cb, PDO::PARAM_STR);
        $modifier_infos_client->bindParam(":num_cb", $num_cb, PDO::PARAM_STR);
        $modifier_infos_client->bindParam(":mois_valid", $mois_valid_cb, PDO::PARAM_STR);
        $modifier_infos_client->bindParam(":an_valid", $annee_valid_cb, PDO::PARAM_STR);
        $modifier_infos_client->bindParam(":num_secu", $num_secu_cb, PDO::PARAM_STR);
        $modifier_infos_client->bindParam(":newsletter", $newsletter, PDO::PARAM_INT);
        $modifier_infos_client->bindParam(":id_client", $id, PDO::PARAM_INT);

        $modifier_infos_client->execute();
        $modifier_infos_client->closeCursor();
        /* ----------------------------------------------------------------------------------------------------------------------
       --------- PREPARATION DE LA REQUETE : INSERTION DANS LA TABLE NEWSLETTER SI LA VALEUR DE NEWSLETTER = 1 cad. TRUE --------
       -------------------------------------------------------------------------------------------------------------------------- */
       if($newsletter == 1 && existe($email)==0){
          $donnees = 'INSERT INTO newsletter (email, date_inscription) VALUES (:email, NOW())';
          $req = $bdd->prepare($donnees);
          $req->bindParam(":email", $mail, PDO::PARAM_STR);
          $req->execute();
          $req->closeCursor();
       }

        // On force le rafraîchissement de la page afin d'afficher les nouvelles informations
        header("Refresh:0");
    }
    else
        $erreurs[]= "Les champs doivent contenir quelque chose";

    return $erreurs;
}
