    <footer>
      <div class="container">
        <div class="narrow">
          <ul>
            <li><a href="www.google.com">Contatti</a></li>
            <li><a href="www.google.com">FAQ</a></li>
            <li><a href="www.google.com">Iscrizione newsletter</a></li>
            <li>
              <a href="www.google.com">Modalità di pagamento</a>
              <ul class="payment-methods">
                <li><img class="payment-method" src="images/mastercard.png" alt="Modalità di pagamento- MasterCard"></li>
                <li><img class="payment-method" src="images/visa.png" alt="Modalità di pagamento- Visa"></li>
                <li><img class="payment-method" src="images/paypal.png" alt="Modalità di pagamento- PayPal"></li>
                <li><img class="payment-method" src="images/americanexpress.png" alt="Modalità di pagamento- AmericanExpress"></li>
              </ul>
            </li>
          </ul>
        </div>
        <div class="narrow">
          <ul>
            <li><a href="www.google.com">Tariffe spedizioni</a></li>
            <li><a href="www.google.com">Condizioni generali di vendita</a></li>
            <li><a href="www.google.com">Note legali</a></li>
            <li><a href="www.google.com">Cookies</a></li>
          </ul>
        </div>
        <div class="narrow">
          <ul>
            <li>
              Chi siamo
              <ul>
                <li><a href="www.google.com">La famiglia</a></li>
                <li><a href="www.google.com">I nostri vini</a></li>
                <li><a href="www.google.com">Chi siamo</a></li>
                <li><a href="www.google.com">Premi e riconoscimenti</a></li>
                <li><a href="www.google.com">Il territorio</a></li>
              </ul>
            </li>
            <li>
              Prodotti
              <ul>
                <li><a href="www.google.com">Crémant d'Alsace</a></li>
                <li><a href="www.google.com">Alsace Tradition</a></li>
                <li><a href="www.google.com">Alsace Lieux-Dits</a></li>
                <li><a href="www.google.com">Alsace Grands Crus</a></li>
                <li><a href="www.google.com">Alsace Pinot Noir</a></li>
                <li><a href="www.google.com">Cuvée Prestige</a></li>
                <li><a href="www.google.com">Vendages Tardives et Sélections de Grains Nobles</a></li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </footer>
  </body>
</html>
