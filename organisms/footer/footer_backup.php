<footer>
  <div class="container">
    <div class="narrow">
      <ul>
        <li><a href="contatti/contatti.php">Contatti</a></li>
        <li><a href="legal-information/FAQ/FAQ.php">FAQ</a></li>
        <li>
          Iscrizione newsletter:<br>
          <input class="newsletter-input" type="email" name="newsletter-input" placeholder="Indirizzo e-mail">
          <input class="newsletter-submit" type="button" name="newsletter-submit" value="Inviare"/>
        </li>
        <li>
          Modalità di pagamento:
          <ul class="payment-methods">
            <li><img class="payment-method" src="organisms/footer/images/mastercard.png" alt="Modalità di pagamento- MasterCard"></li>
            <li><img class="payment-method" src="organisms/footer/images/visa.png" alt="Modalità di pagamento- Visa"></li>
            <li><img class="payment-method" src="organisms/footer/images/americanexpress.png" alt="Modalità di pagamento- AmericanExpress"></li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="narrow">
      <ul>
        <li><a href="legal-information/tariffe-spedizioni/tariffe-spedizioni.php">Tariffe spedizioni</a></li>
        <li><a href="legal-information/condizioni-generali-di-vendita/condizioni-generali-di-vendita.php">Condizioni generali di vendita</a></li>
        <li><a href="legal-information/note-legali/note-legali.php">Note legali</a></li>
        <li><a href="legal-information/cookies/cookies.php">Cookies</a></li>
      </ul>
    </div>
    <div class="narrow">
      <ul>
        <li>
          Chi siamo
          <ul>
            <li><a href="chi-siamo/la-famiglia/la-famiglia.php">La famiglia</a></li>
            <li><a href="chi-siamo/i-nostri-vini/i-nostri-vini.php">I nostri vini</a></li>
            <li><a href="chi-siamo/premi-e-riconoscimenti/premi-e-riconoscimenti.php">Premi e riconoscimenti</a></li>
            <li><a href="chi-siamo/il-territorio/il-territorio.php">Il territorio</a></li>
          </ul>
        </li>
        <li>
          Prodotti
          <ul>
            <li><a href="http://www.google.com">Crémant d'Alsace</a></li>
            <li><a href="http://www.google.com">Alsace Tradition</a></li>
            <li><a href="http://www.google.com">Alsace Lieux-Dits</a></li>
            <li><a href="http://www.google.com">Alsace Grands Crus</a></li>
            <li><a href="http://www.google.com">Alsace Pinot Noir</a></li>
            <li><a href="http://www.google.com">Cuvée Prestige</a></li>
            <li><a href="http://www.google.com">Vendages Tardives et Sélections de Grains Nobles</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</footer>
</body>
</html>
