<?php if(isset($_SESSION['client'])): ?>
  <!-- affiche le menu suivant si la session client existe -->
 <ul>
  <li>
    <img src="/organisms/header/images/consegna.svg" alt="" /><p>Consegna</p>
  </li>
  <li>
    <img src="/organisms/header/images/login.svg" alt="" /><p><a href="areaclienti/account.php">Account</a></p>
  </li>
  <li>
    <img src="/organisms/header/images/carrello.svg" alt="" /><p>Carrello</p>
  </li>
</ul>
<?php else: ?>
  <!-- affiche le menu suivant si la session client n'existe pas -->
  <ul>
   <li>
     <img src="/organisms/header/images/consegna.svg" alt="" /><p>Consegna</p>
   </li>
   <li>
     <img src="/organisms/header/images/login.svg" alt="" /><p><a href="areaclienti/login.php">Login</a></p>
   </li>
   <li>
     <img src="/organisms/header/images/carrello.svg" alt="" /><p>Carrello</p>
   </li>
 </ul>
<?php endif; ?>
