<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8">
    <title>Koehly produttore e venditore online di vini di Alsazia</title>
    <link rel="stylesheet" type="text/css" href= "/style.css" />
    <link href="https://fonts.googleapis.com/css?family=Arbutus+Slab" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/main.js"></script>
  </head>
  <body>
  <header>
    <div class="shopping-bar">
      <div class="container">
        <?php include 'shopping-bar/shopping-bar.php';?>
      </div>
    </div>
    <div class="navigation">
      <div class="container">
        <ul>
          <li><a href="../../index.php">Home</a></li>
          <li><a href="../../chi-siamo/chi-siamo.php">Chi siamo</a></li>
          <li><a href="../../vini/vini.php">Vini</a></li>
          <li><a href="../../contatti/contatti.php">Contatti</a></li>
          <div class="search-field">
        		<form method="get" action="http://www.google.com">
        		    <input class="text-field" type="text" size="11" maxlength="120">
                <input class="search-button" type="submit" value="&#x2315;">
        		</form>
        	</div>
        </ul>
      </div>
    </div>
    <div class="container">
      <img class="logo-image" src="organisms/header/images/logo.svg" alt="" />
    </div>
  </header>
