<?php
    /* SCRIPT PHP NECESSAIREMENT EN TETE DE TOUTES LES PAGES WEB.
      le session_start() doit impérativement être la première instruction PHP en haut de page.
    */
    session_start();

    require_once '../../functions/admin.php';
    require_once '../../functions/bdd.php';
    $bdd = bdd();
    $produit = produit($_GET["id"]);
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8">
    <title>Koehly produttore e venditore online di vini di Alsazia</title>
    <link rel="stylesheet" type="text/css" href= "../../style.css" />
    <link href="https://fonts.googleapis.com/css?family=Arbutus+Slab" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/main.js"></script>
  </head>
<body>
<header>
  <div class="shopping-bar">
    <div class="container">
      <?php include '../../organisms/header/shopping-bar/shopping-bar-niv-2.php' ?>
    </div>
  </div>
  <div class="navigation">
    <div class="container">
<<<<<<< HEAD
      <a href="../../index.php"><img class="logo-image" src="../../organisms/header/images/logo.svg" alt="" /></a>
=======
      <?php include '../../organisms/header/header-niv-2.php' ?>
      </ul>
>>>>>>> origin/maral
    </div>
  </div>
  <div class="container">
    <a href="index.php"><img class="logo-image" src="../../../organisms/header/images/logo.svg" alt="" /></a>
  </div>
</header>
<div class="prodotto-page">
  <div class="main container">
    <div class="container product-sheet">

      <div class="narrow">
        <input class="go-back" type="button" onclick="location.href='javascript:history.go(-1)';" value="&#8592; Tornare indietro" />
        <img class="product-image" src="../../img/products/<?= $produit["photo_vin"]?>" alt="<?php $produit["photo_alt"]?>">
      </div>
      <div class="wide">
        <!-- fil d'ariane -->
        <p> <a href="../../index.php" title="home page">Home</a> / <a href="../products-page/prodotti.php" title="I nostri vini e crémants">Vini</a> / <?= $produit['lib_cat'] ?></p>
        <!-- récupère la valeur du libellé pour le mettre à la place du H1 -->
        <h1><?= $produit['libelle_vin'] ?></h1>
        <!--
        Complexité :
        Etant donné la façon dont est affiché le prix,
        je dois récupérer le prix avec les unités et les centimes (deux chiffres après la virgule)
        les centimes étant stockées entre les balises <sup></sup>
        Je dois exploser la valeur récupérée en BDD -->
        <h2><?= number_format($produit['prix_vin'], 2, '.', '') ?> &euro;</h2>
        <!--
        Je mets en commentaire ce que tu as fait pour l'instant pour pouvoir afficher simplement le prix.
        Dès que j'aurais trouvé la manière d'exploser les valeurs récupérées en bdd,
        je pourrai intégrer les valeurs du prix la forme $produit['prix_vin']-> unité et $produit['prix_vin']-> centimes
        <h2>7<sup>70</sup>&euro;</h2>
        -->
        <!-- Les quantités disponibles -->
        <input class="quantity" type="number" min="1" max="90" step="1" value="1">
        <!-- bouton ajouter au panier -->
        <input class="button" type="button" value="Aggiungere">
        <p class="availability">Disponibilità:</p>
        <div class="tabs-container">
          <ul class="tabs">
            <li class="tab-link current" data-tab="tab-1">Caratteristiche</li>
            <li class="tab-link" data-tab="tab-2">Gastronomia</li>
          </ul>
          <div id="tab-1" class="tab-content current">
            <table>
              <tr>
                <th>
                  <div>
                    <img class="product-sheet-icon" src="/vini/product-sheet/images/icon-classificazione.svg" alt="Classificazione">
                    <!-- étant donné que la dénomination contrôlée Alsace est la même pour tous les produits
                    Je ne l'ai pas inséré dans la BDD
                    Selon Monsieur Chouali, on n'a pas à s'embêter à stocker cette valeur en BDD. La laisser en fixe suffit.
                     -->
                    Classificazione: Denominazione Controllata Alsazia
                  </div>
                </th>
                <th>
                  <div>
                    <img class="product-sheet-icon" src="/vini/product-sheet/images/icon-gradazione.svg" alt="Gradazione">
                    Gradazione: <?= $produit['degre_vin'] ?>
                  </div>
                </th>
              </tr>
              <tr>
                <th>
                  <div>
                    <img class="product-sheet-icon" src="/vini/product-sheet/images/icon-produzione.svg" alt="Produzione">
                    Zona di produzione: <?= $produit['zone_prod_vin'] ?>
                  </div>
                </th>
                <th>
                  <div>
                    <img class="product-sheet-icon" src="/vini/product-sheet/images/icon-temperatura.svg" alt="Temperatura">
                    Temperatura: <?= $produit['temperature_service'] ?> &deg;
                  </div>
                </th>
              </tr>
              <tr>
                <th>
                  <div>
                    <img class="product-sheet-icon" src="/vini/product-sheet/images/icon-uva.svg" alt="Uva">
                    Uva: <?= $produit['raisin_vin'] ?>
                  </div>
                </th>
                <th>
                  <div>
                    <img class="product-sheet-icon" src="/vini/product-sheet/images/icon-capacita.svg" alt="Capacita">
                    Capacità: <?= $produit['contenance_vin'] ?>cl
                  </div>
                </th>
              </tr>
              <tr>
                <th>
                  <div>
                    <img class="product-sheet-icon" src="/vini/product-sheet/images/icon-lavorazione.svg" alt="Lavorazione">
                    Lavorazione: <?= $produit['recolte_raisin'] ?>
                  </div>
                </th>
                <th>
                  <div>
                    <img class="product-sheet-icon" src="/vini/product-sheet/images/icon-affinamento.svg" alt="Affinamento">
                    Affinamento: <?= $produit['affinage_vin'] ?>
                  </div>
                </th>
              </tr>
              <tr>
                <th>
                  <div>
                    <img class="product-sheet-icon" src="/vini/product-sheet/images/icon-propreta.svg" alt="Propreta">
                    Proprietà: <?= $produit['proprietes_vin'] ?>
                  </div>
                </th>
                <th></th>
              </tr>
            </table>
          </div>
          <div id="tab-2" class="tab-content">
            <!-- mit sous forme de liste à la base. Mais à ajuster selon la maquette
            problème constaté : au scroll impossible de cliquer sur les onglets caratterische et gastronomia
            C'est sûrement dû à un problème de barre de navigation. -->
            <ul>
                <li>
                    <p>Degustazione:</p>
                    <p><?= $produit['degustation_vin'] ?></p>
                </li>
                <li>
                    <p>Abbinamenti:</p>
                    <p><?= $produit['mariage_vin'] ?></p>
                </li>
            </ul>
        </div>
      </div>
    </div>
  </div>


  <div class="container bestsellers-section">
    <h2>
      Le nostre migliori vendite
    </h2>
    <figure class="narrow">
      <img class="bestsellers-image" src="../../index/section-bestsellers/images/Cremant_d_Alsace_BLANC_DE_NOIRS_2013.jpg" alt="" />
      <figcaption>
        <h3>Crémant St-Urbain rosé</h3>
        <h4>8.70 &euro;</h4>
      </figcaption>
      <input class="button" type="button" value="Più informazioni">
      <input class="button" type="button" value="Aggiungere">
    </figure>
    <figure class="narrow">
      <img class="bestsellers-image" src="../../index/section-bestsellers/images/Cremant_d_Alsace_ST_URBAINE_2013.jpg" alt="" />
      <figcaption>
        <h3>Crémant St-Urbain brut Riesling</h3>
        <h4>7.70 &euro;</h4>
      </figcaption>
      <input class="button" type="button" value="Più informazioni">
      <input class="button" type="button" value="Aggiungere">
    </figure>
    <figure class="narrow">
      <img class="bestsellers-image" src="../../index/section-bestsellers/images/Cremant_d_Alsace_ST_URBAINE_ROSE_2013.jpg" alt="" />
      <figcaption>
        <h3>Crémant Blanc de Noir brut</h3>
        <h4>8.50 &euro;</h4>
      </figcaption>
      <input class="button" type="button" value="Più informazioni">
      <input class="button" type="button" value="Aggiungere">
    </figure>
  </div>



</div>

<div class="fullwidth-image">

</div>
</div>

<footer>
<div class="container">
  <div class="narrow">
    <ul>
      <li><a href="../../contatti/contatti.php">Contatti</a></li>
      <li><a href="../../legal-information/FAQ/FAQ.php">FAQ</a></li>
      <li>
        Iscrizione newsletter:<br>
        <input class="newsletter-input" type="email" name="newsletter-input" placeholder="Indirizzo e-mail">
        <input class="newsletter-submit" type="button" name="newsletter-submit" value="Inviare"/>
      </li>
      <li>
        Modalità di pagamento:
        <ul class="payment-methods">
          <li><img class="payment-method" src="../../organisms/footer/images/mastercard.png" alt="Modalità di pagamento- MasterCard"></li>
          <li><img class="payment-method" src="../../organisms/footer/images/visa.png" alt="Modalità di pagamento- Visa"></li>
          <li><img class="payment-method" src="../../organisms/footer/images/americanexpress.png" alt="Modalità di pagamento- AmericanExpress"></li>
        </ul>
      </li>
    </ul>
  </div>
  <div class="narrow">
    <ul>
      <li><a href="../../legal-information/tariffe-spedizioni/tariffe-spedizioni.php">Tariffe spedizioni</a></li>
      <li><a href="../../legal-information/condizioni-generali-di-vendita/condizioni-generali-di-vendita.php">Condizioni generali di vendita</a></li>
      <li><a href="../../legal-information/note-legali/note-legali.php">Note legali</a></li>
      <li><a href="../../legal-information/cookies/cookies.php">Cookies</a></li>
    </ul>
  </div>
  <div class="narrow">
    <ul>
      <li>
        Chi siamo
        <ul>
          <li><a href="../../chi-siamo/la-famiglia/la-famiglia.php">La famiglia</a></li>
          <li><a href="../../chi-siamo/i-nostri-vini/i-nostri-vini.php">I nostri vini</a></li>
          <li><a href="../../chi-siamo/premi-e-riconoscimenti/premi-e-riconoscimenti.php">Premi e riconoscimenti</a></li>
          <li><a href="../../chi-siamo/il-territorio/il-territorio.php">Il territorio</a></li>
        </ul>
      </li>
      <li>
        Prodotti
        <ul>
          <li><a href="http://www.google.com">Crémant d'Alsace</a></li>
          <li><a href="http://www.google.com">Alsace Tradition</a></li>
          <li><a href="http://www.google.com">Alsace Lieux-Dits</a></li>
          <li><a href="http://www.google.com">Alsace Grands Crus</a></li>
          <li><a href="http://www.google.com">Alsace Pinot Noir</a></li>
          <li><a href="http://www.google.com">Cuvée Prestige</a></li>
          <li><a href="http://www.google.com">Vendages Tardives et Sélections de Grains Nobles</a></li>
        </ul>
      </li>
    </ul>
  </div>
</div>
</footer>
</body>
</html>
