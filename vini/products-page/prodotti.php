<?php

    /* SCRIPT PHP NECESSAIREMENT EN TETE DE TOUTES LES PAGES WEB.
      le session_start() doit impérativement être la première instruction PHP en haut de page.
    */
    session_start();
    require_once '../../functions/bdd.php';
    require_once '../../functions/admin.php';
    $bdd = bdd();
    $produits = produits();
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8">
    <title>I nostri prodotti | Koehly</title>
    <link rel="stylesheet" type="text/css" href= "../../style.css" />
    <link href="https://fonts.googleapis.com/css?family=Arbutus+Slab" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/main.js"></script>
  </head>
<body>
  <h1>I nostri vini e crémants</h1>

  <header>
    <div class="shopping-bar">
      <div class="container">
        <?php include '../../organisms/header/shopping-bar/shopping-bar-niv-2.php' ?>
      </div>
    </div>
    <div class="navigation">
      <div class="container">
        <?php include '../../organisms/header/header-niv-2.php' ?>
        </ul>
      </div>
    </div>
    <div class="container">
      <a href="index.php"><img class="logo-image" src="../../../organisms/header/images/logo.svg" alt="" /></a>
    </div>
  </header>
  <div>
    <!--  affichage sous forme de tableau pour le test.
     Je pense que ce n'est pas adapté.
    A voir pour une présentation sous forme de liste -->
      <table border="1" style="border-collapse:collapse;">
          <thead>
              <tr>
                  <td>Référence</td>
                  <td>Libéllé</td>
                  <td>Catégorie</td>
                  <td>Prix à l'unité</td>
                  <td>Visualiser</td>
              </tr>

          </thead>
          <tbody>
              <?php
                  foreach($produits as $produit):
                      // on place le html entre les deux morceaux de php
              ?>
              <tr>
                  <td><?= $produit['ref_vin'] ?></td>
                  <td><?= $produit['libelle_vin'] ?></td>
                  <td><?= $produit['lib_cat'] ?></td>
                  <td><?= number_format($produit['prix_vin'], 2, '.', '') ?> &euro;</td>
                  <td><a href="../product-sheet/prodotto.php?id=<?= $produit['ref_vin'] ?>?cat=<?= $produit['id_cat']?>">Visualiser</a></td>
              </tr>
              <?php
               endforeach;
              ?>
          </tbody>
      </table>
  </div>

</body>
</html>
