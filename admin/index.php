<?php
    require_once '../functions/bdd.php';
    require_once '../functions/admin.php';
    $bdd = bdd();
    if(!empty($_POST))
        $erreur = connexion();
?>
   <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <title>Admin | Login</title>
    </head>

    <body>
        <h1>Espace de connexion</h1>
         <?php
                    if(isset($erreur)):
                ?>
                <p><?= $erreur ?></p>
                <?php
                    endif;
                ?>
        <form method="post">
            <label for="email">E-mail</label>
            <input id="email" name="email" type="text" value="<?php if(isset($_POST["email"])) echo $_POST["email"]; ?>">
            <label for="password">Mot de passe</label>
            <input id="password" name="password" type="password">
            <input type="submit" value="connexion">
        </form>
    </body>

    </html>
