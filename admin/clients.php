<?php
    session_start();
    require_once '../functions/bdd.php';
    require_once '../functions/admin.php';
    $bdd = bdd();
    $clients = clients();
    $nombreclients = nombre_clients();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Dashboard</title>
    </head>

    <body>
       <?php include_once 'header.php' ?>
       <h1>Liste des clients</h1>
       Nombre de clients : <?= $nombreclients ?>
       <table border="1" style="border-collapse:collapse;">
         <thead>
           <tr>
             <td>Nom</td>
             <td>Prénom</td>
             <td>Adresse</td>
             <td>Mail</td>
             <td>Téléphone</td>
             <td>Date d'inscription</td>
             <td>Reçoit la newsletter ?</td>
           </tr>
         </thead>
         <tbody>
        <?php
         foreach ($clients as $client):
        ?>
         <tr>
           <td><?= $client['nom_client'] ?></td>
           <td><?= $client['prenom_client'] ?></td>
           <td>
             <ul>
               <li><?= $client['adresse_client'] ?></li>
               <li><?= $client['cp_client'] ?></li>
               <li><?= $client['ville_client'] ?></li>
             </ul>
           </td>
           <td><?= $client['mail_client'] ?></td>
           <td><?= $client['tel_client'] ?></td>
           <td><?= $client['date'] ?></td>
           <td><?php if($client['newsletter']==1) echo "Oui"; else echo "Non"; ?></td>
         </tr>
        <?php endforeach; ?>
        </tbody>
       </table>
    </body>

</html>
