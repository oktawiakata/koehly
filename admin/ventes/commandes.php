<?php
    session_start();
    require_once '../../functions/bdd.php';
    require_once '../../functions/admin.php';
    $bdd = bdd();
    $commandes = commandes();
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <title>Commandes</title>
        <!-- CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!-- JS -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    </head>

    <body>
        <h1>Bienvenue dans l'espace administateur.</h1>
        <nav>
            <ul>
                <li><a href="../produits/liste.php">Catalogue</a></li>
                <li><a href="../ventes/commandes.php">Ventes</a></li>
                <li><a href="../clients.php">Clients</a></li>
                <li><a href="../deconnexion.php">Déconnexion</a></li>
            </ul>
        </nav>

        <div>
            <table border="1" style="border-collapse:collapse;">
                <thead>
                    <tr>
                        <td>Numéro de commande</td>
                        <td>Date</td>
                        <td>Moyen de paiement</td>
                        <td>Etat paiement</td>
                        <td>Etat commande</td>
                        <td>Total (en €)</td>

                    </tr>

                </thead>
                <tbody>
                    <?php
                        foreach($commandes as $commande):
                            // on place le html entre les deux morceaux de php
                    ?>
                    <tr>
                        <td><?= $commande['num_commande'] ?></td>
                        <td><?= $commande['date_commande'] ?></td>
                        <td><?= $commande['moyen_paiement'] ?></td>
                        <td><?= $commande['etat_paiement'] ?></td>
                        <td><?= $commande['etat_commande'] ?></td>
                        <td><?= number_format($commande['total_com'], 2, ',', ' ') ?> &euro;</td>
                    </tr>

                    <?php
                     endforeach;
                    ?>
                </tbody>
            </table>
        </div>


    </body>
    </html>
