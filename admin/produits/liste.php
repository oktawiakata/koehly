<?php
    session_start();
    require_once '../../functions/bdd.php';
    require_once '../../functions/admin.php';
    $bdd = bdd();
    $produits = produits();
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <title>Dashboard</title>
        <!-- CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <!-- JS -->
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>

          $(document).ready(function() {
            var theHREF;

            $( "#dialog-confirm" ).dialog({
                resizable: false,
                height:160,
                width:500,
                autoOpen: false,
                modal: true,
                buttons: {
                    "Oui": function() {
                        $( this ).dialog( "close" );
                        window.location.href = theHREF;
                    },
                    "Annuler": function() {
                        $( this ).dialog( "close" );
                    }
                }
            });

            $("a.confirmModal").click(function(e) {
                e.preventDefault();
                theHREF = $(this).attr("href");
                $("#dialog-confirm").dialog("open");
            });
          })
        </script>
    </head>

    <body>
        <h1>Bienvenue dans l'espace administateur.</h1>
        <nav>
            <ul>
                <li><a href="liste.php">Catalogue</a></li>
                <li><a href="../ventes/commandes.php">Ventes</a></li>
                <li><a href="../clients.php">Clients</a></li>
                <li><a href="../deconnexion.php">Déconnexion</a></li>
            </ul>
        </nav>

        <div>
            <table border="1" style="border-collapse:collapse;">
                <thead>
                    <tr>
                        <td>Référence</td>
                        <td>Libéllé</td>
                        <td>Catégorie</td>
                        <td>Prix à l'unité</td>
                        <td>Visualiser</td>
                        <td>Editer</td>
                        <td>Supprimer</td>
                    </tr>

                </thead>
                <tbody>
                    <?php
                        foreach($produits as $produit):
                            // on place le html entre les deux morceaux de php
                    ?>
                    <tr>
                        <td><?= $produit['ref_vin'] ?></td>
                        <td><?= $produit['libelle_vin'] ?></td>
                        <td><?= $produit['lib_cat'] ?></td>
                        <td><?= number_format($produit['prix_vin'], 2, '.', '') ?> &euro;</td>
                        <td><a href="../../vini/product-sheet/prodotto.php?id=<?= $produit['ref_vin'] ?>?cat=<?= $produit['id_cat']?>" target="_blank"><i class="fa fa-eye" aria-hidden="true"></i></a></td>
                        <td><a href="modifier.php?id=<?= $produit['ref_vin'] ?>"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a></td>
                        <td><a class="confirmModal" href="supprimer.php?id=<?= $produit['ref_vin']?>"><i class="fa fa-trash-o" aria-hidden="true"></i></a></td>
                    </tr>

                    <?php
                     endforeach;
                    ?>
                </tbody>
            </table>

            <p><a href="ajouter.php" >Créer un nouveau produit</a></p>
            <!-- boîte modale de confirmation de suppression du produit -->
            <div id="dialog-confirm" title="Confirmation de la suppression" style="display:none; overflow:hidden;">

              <span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>
              <p>Etes-vous sûr de vouloir supprimer ce produit du catalogue ?</p>

            </div>
            <!-- fin boite modale de confirmation de suppression du produit -->
        </div>


    </body>
    </html>
