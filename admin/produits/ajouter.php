<?php
    session_start();
    //appel aux fichers de fonctions PHP
    require_once '../../functions/admin.php';
    require_once '../../functions/bdd.php';
    $bdd = bdd();
    //affichage des catégories dans les values des options présents dans le select
    $categories = categories();
    // ajouter un produit
    if(!empty($_POST))
        $erreurs = ajouter();
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <title>Ajouter | Produit</title>
        <style>
            ul li {
                list-style-type: none;
            }

            input, select {
                display: block;
                width: 300px;
                height: 25px;
            }

            textarea {
                display: block;
            }
        </style>
    </head>

    <body>
      <nav>
          <ul>
              <li><a href="liste.php">Catalogue</a></li>
              <li><a href="../ventes/commandes.php">Ventes</a></li>
              <li><a href="../clients.php">Clients</a></li>
              <li><a href="../deconnexion.php">Déconnexion</a></li>
          </ul>
      </nav>
        <h1>Créer un nouveau produit</h1>
            <?php
              if(isset($erreurs)) :
              if($erreurs) :
              foreach($erreurs as $erreur) :
              ?>
              <p style="color:red"><?= $erreur ?></p>
              <?php
              endforeach;
              else :
              ?>
              <p style="color:green">Produit ajouté !</p>
              <p><a href="liste.php">Cliquez ici pour retourner au catalogue</a>.</p>
              <?php
              endif;
              endif;
              ?>
          <p><a href="liste.php">Liste des produits</a></p>
        <form method="post" action="" enctype="multipart/form-data">
            <ul>
                <li>
                    <label for="libelle">Libellé :</label>
                    <input id="libelle" name="libelle" type="text" value="<?php if(isset($_POST['libelle'])) echo $_POST['libelle']; ?>">
                </li>
                <li>
                    <label for="prix">Prix (en €, à l'unité) :</label>
                    <input id="prix" name="prix" type="text" value="<?php if(isset($_POST['prix'])) echo $_POST['prix']; ?>">
                </li>
                <!-- catégorie -->
                <li>
                    <label for="categorie">Catégorie :</label>
                    <select id="categorie" name="categorie">
                           <option value="">Sélectionner une catégorie</option>
                        <?php
                            foreach($categories as $categorie):

                        ?>
                            <option value="<?= $categorie['id_cat']?>"><?= $categorie['id_cat'] ?> - <?= $categorie['lib_cat']?></option>
                        <?php
                            endforeach;
                        ?>
                    </select>
                </li>
            </ul>
            <h2>Caratteristiche</h2>
            <ul>
                <li>
                    <p>Classificazione:</p>
                    <p>Denominazione Controllata Alsazia</p>
                </li>
                <li>
                    <label for="zoneprod">Zone de production :</label>
                    <input id="zoneprod" name="zoneprod" type="text" value="<?php if(isset($_POST['zoneprod'])) echo $_POST['zoneprod']; ?>">
                </li>
                <li>
                    <label for="typeraisin">Type de raisin :</label>
                    <input id="typeraisin" name="typeraisin" type="text" value="<?php if(isset($_POST['typeraisin'])) echo $_POST['typeraisin']; ?>">
                </li>
                <li>
                    <label for="recolteraisin">Récolte du raisin :</label>
                    <input id="recolteraisin" name="recolteraisin" type="text" value="<?php if(isset($_POST['recolteraisin'])) echo $_POST['recolteraisin']; ?>">
                </li>
                 <li>
                    <label for="affinage">Mode d'affinage :</label>
                    <input id="affinage" name="affinage" type="text" value="<?php if(isset($_POST['affinage'])) echo $_POST['affinage']; ?>">
                </li>
                <li>
                    <label for="degrevin">Degré alcoolique :</label>
                    <input id="degrevin" name="degrevin" type="text" value="<?php if(isset($_POST['degrevin'])) echo $_POST['degrevin']; ?>">
                </li>
                <li>
                    <label for="temperature">Température de service (en °C) :</label>
                    <input id="temperature" name="temperature" type="text" value="<?php if(isset($_POST['temperature'])) echo $_POST['temperature']; ?>">
                </li>
                <li>
                    <label for="proprietes">Propriétés du vin :</label>
                    <input id="proprietes" name="proprietes" type="text" value="<?php if(isset($_POST['proprietes'])) echo $_POST['proprietes']; ?>">
                </li>
                <li>
                    <label for="contenance">Contenance (en cl) :</label>
                    <input id="contenance" name="contenance" type="text" value="<?php if(isset($_POST['contenance'])) echo $_POST['contenance']; ?>">
                </li>
                <li>
                    <label for="millesime">Millésime :</label>
                    <input id="millesime" name="millesime" type="text" value="<?php if(isset($_POST['millesime'])) echo $_POST['millesime']; ?>">
                </li>
            </ul>
            <h2>Gastronomia</h2>
            <ul>
                <li>
                    <label for="degustation">Dégustation :</label>
                    <textarea id="degustation" name="degustation" rows="10" cols="60"><?php if(isset($_POST['degustation'])) echo $_POST['degustation']; ?></textarea>

                </li>
                <li>

                    <label for="mariage">Mariage :</label>
                    <textarea id="mariage" name="mariage" rows="10" cols="60"><?php if(isset($_POST['mariage'])) echo $_POST['mariage']; ?></textarea>
                </li>
            </ul>
            <h2>Quantités :</h2>
            <ul>
                <li>
                    <label for="qtedispo">Quantité disponible (par défaut mettre 50) :</label>
                    <input id="qtedispo" name="qtedispo" type="text" value="<?php if(isset($_POST['qtedispo'])) echo $_POST['qtedispo']; ?>">

                </li>
            </ul>
            <h2>Image produit</h2>
            <ul>
              <li>
                <input type="hidden" name="MAX_FILE_SIZE" value="300000">
                <input type="file" name="file">
              </li>
              <li>
                <label for="altattribute">Texte alternatif de l'image :</label>
                <input id="altattribute" name="altattribute" type="text" value="<?php if(isset($_POST['altattribute'])) echo $_POST['altattribute'];?>">
              </li>
            </ul>
            <input type="submit" value="Ajouter">
        </form>
    </body>

    </html>
