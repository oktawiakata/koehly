<?php
    session_start();
    require_once '../../functions/admin.php';
    require_once '../../functions/bdd.php';
    $bdd = bdd();
    $produit = produit($_GET["id"]);
    //affichage des catégories dans les values des options présents dans le select
    $categories = categories();
    if(!empty($_POST))
        $erreurs = modifier();
?>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <title>Modifier | Produit</title>
        <style>
            ul li {
                list-style-type: none;
            }

            input {
                display: block;
                width: 300px;
                height: 25px;
            }

            textarea {
                display: block;
            }
        </style>
    </head>

    <body>
        <nav>
            <ul>
                <li><a href="liste.php">Catalogue</a></li>
                <li><a href="../ventes/commandes.php">Ventes</a></li>
                <li><a href="../clients.php">Clients</a></li>
                <li><a href="../deconnexion.php">Déconnexion</a></li>
            </ul>
        </nav>
        <p><a href="liste.php">Liste des produits</a></p>
        <h1>Modifier la fiche produit</h1>
        <?php
          if(isset($erreurs)) :
          if($erreurs) :
          foreach($erreurs as $erreur) :
          ?>
          <p style="color:red"><?= $erreur ?></p>
          <?php
          endforeach;
          else :
          endif;
          endif;
          ?>

        <form method="post" action="" enctype="multipart/form-data">
            <ul>
                <li>
                    <label for="libelle">Libellé :</label>
                    <input id="libelle" name="libelle" type="text" value="<?= $produit['libelle_vin'] ?>">
                </li>
                <li>
                    <label for="prix">Prix (en €, à l'unité) :</label>
                    <input id="prix" name="prix" type="text" value="<?= $produit['prix_vin'] ?>">
                </li>
                <!-- catégorie -->
                <li>
                    <label for="categorie">Catégorie :</label>
                    <select id="categorie" name="categorie">
                           <option value="">Sélectionner une catégorie</option>
                        <?php
                            foreach($categories as $categorie):

                        ?>
                            <option value="<?= $categorie['id_cat']?>"<?php if($categorie['id_cat'] == $produit['id_cat']) echo "selected"?>><?= $categorie['id_cat'] ?> - <?= $categorie['lib_cat']?></option>
                        <?php
                            endforeach;
                        ?>
                    </select>
                </li>

            </ul>
            <h2>Caratteristiche</h2>
            <ul>
                <li>
                    <p>Classificazione:</p>
                    <p>Denominazione Controllata Alsazia</p>
                </li>
                <li>
                    <label for="zoneprod">Zone de production :</label>
                    <input id="zoneprod" name="zoneprod" type="text" value="<?= $produit['zone_prod_vin'] ?>">
                </li>
                <li>
                    <label for="typeraisin">Type de raisin :</label>
                    <input id="typeraisin" name="typeraisin" type="text" value="<?= $produit['raisin_vin'] ?>">
                </li>
                <li>
                    <label for="recolteraisin">Récolte du raisin :</label>
                    <input id="recolteraisin" name="recolteraisin" type="text" value="<?= $produit['recolte_raisin'] ?>">
                </li>
                <li>
                    <label for="affinage">Mode d'affinage :</label>
                    <input id="affinage" name="affinage" type="text" value="<?= $produit['affinage_vin'] ?>">
                </li>
                <li>
                    <label for="degrevin">Degré alcoolique :</label>
                    <input id="degrevin" name="degrevin" type="text" value="<?= $produit['degre_vin'] ?>">
                </li>
                <li>
                    <label for="temperature">Température de service (en °C) :</label>
                    <input id="temperature" name="temperature" type="text" value="<?= $produit['temperature_service'] ?>">
                </li>
                <li>
                    <label for="proprietes">Propriétés du vin :</label>
                    <input id="proprietes" name="proprietes" type="text" value="<?= $produit['proprietes_vin'] ?>">
                </li>
                <li>
                    <label for="contenance">Contenance (en cl) :</label>
                    <input id="contenance" name="contenance" type="number" value="<?= $produit['contenance_vin'] ?>">
                </li>
                <li>
                    <label for="millesime">Millésime :</label>
                    <input id="millesime" name="millesime" type="number" value="<?= $produit['millesime_vin'] ?>">
                </li>
            </ul>
            <h2>Gastronomia</h2>
            <ul>
                <li>
                    <label for="degustation">Dégustation :</label>
                    <textarea id="degustation" name="degustation" rows="10" cols="60"><?= $produit['degustation_vin'] ?>
                    </textarea>

                </li>
                <li>

                    <label for="mariage">Mariage :</label>
                    <textarea id="mariage" name="mariage" rows="10" cols="60"><?= $produit['mariage_vin'] ?>
                    </textarea>
                </li>
            </ul>
            <h2>Quantités :</h2>
            <ul>
                <li>
                    <label for="qtedispo">Quantité disponible (par défaut mettre 50) :</label>
                    <input id="qtedispo" name="qtedispo" type="number" value="<?= $produit['quantite_disponible'] ?>">

                </li>
            </ul>
            <h2>Image produit</h2>
            <ul>
              <li>
                  <input type="text" name="filename" value=<?= $produit["photo_vin"]?>>
                  <p>Pour changer de fichier, charger votre nouvelle image :</p>
                  <input type="hidden" name="MAX_FILE_SIZE" value="3000000">
                  <input type="file" name="file">
              </li>
              <li>
                <label for="altattribute">Texte alternatif de l'image :</label>
                <input id="altattribute" name="altattribute" type="text" value="<?= $produit['photo_alt'] ?>">
              </li>
            </ul>
            <input type="submit" value="Modifier">
        </form>
    </body>

    </html>
