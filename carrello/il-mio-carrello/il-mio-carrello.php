<?php
/* SCRIPT PHP NECESSAIREMENT EN TETE DE TOUTES LES PAGES WEB.
  le session_start() doit impérativement être la première instruction PHP en haut de page.
*/
session_start();
?>
<!DOCTYPE HTML>
<html>
  <head>
    <meta charset="utf-8">
    <title>Koehly produttore e venditore online di vini di Alsazia</title>
    <link rel="stylesheet" type="text/css" href= "../../style.css" />
    <link href="https://fonts.googleapis.com/css?family=Arbutus+Slab" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/main.js"></script>
  </head>
  <body>
    <header>
      <div class="shopping-bar">
        <div class="container">
          <?php include '../../organisms/header/shopping-bar/shopping-bar-niv-2.php' ?>
        </div>
      </div>
      <div class="navigation">
        <div class="container">
          <?php include '../../organisms/header/header-niv-2.php' ?>
          </ul>
        </div>
    </div>
    <div class="container">
      <a href="../../index.php"><img class="logo-image" src="../../organisms/header/images/logo.svg" alt="" /></a>
    </div>
  </header>
<div class="il-mio-carrello-page">
  <div class="main container">
    <input class="go-back" type="button" onclick="location.href='javascript:history.go(-1)';" value="&#8592; Tornare indietro" />
    <p><strong>1. Il mio carrello</strong> / 2. Identificazione / 3. Conferma	/ 4. Pagamento</p>
    <h1>1. Il mio carrello</h1>
    <table>
      <tr>
        <th>Prodotto</th>
        <th></th>
        <th>Prezzo unitario</th>
        <th>Quantità</th>
        <th class="total-costs">Totale</th>
      </tr>
      <tr>
        <td>
          <img src="../../index/section-bestsellers/images/Cremant_d_Alsace_ST_URBAINE_2013.jpg" alt="Crémant St-Urbain brut Riesling">
        </td>
        <td>Crémant St-Urbain brut Riesling</td>
        <td>7.70 &euro;</td>
        <td>
          <input class="quantity" type="number" min="1" max="90" step="1" value="1">
          <img class="remove-item" src="images/trash.png" alt="Eliminare">
        </td>
        <td class="total-costs">7.70 &euro;</td>
      </tr>
      <tr>
        <td>
          <img src="../../index/section-bestsellers/images/Cremant_d_Alsace_ST_URBAINE_ROSE_2013.jpg" alt="Crémant St-Urbain rosé">
        </td>
        <td>Crémant St-Urbain rosé</td>
        <td>8.70 &euro;</td>
        <td>
          <input class="quantity" type="number" min="1" max="90" step="1" value="1">
          <img class="remove-item" src="images/trash.png" alt="Eliminare">
        </td>
        <td class="total-costs">8.70 &euro;</td>
      </tr>
    </table>
    <table class="order-costs">
      <tr>
        <td>Totale parziale</td>
        <td class="total-costs">16.40 &euro;</td>
      </tr>
      <tr>
        <td>Spese di spedizione</td>
        <td class="total-costs">15 &euro;</td>
      </tr>
      <tr>
        <td><strong>Totale ordine</strong></td>
        <td class="total-costs"><strong>31.40 &euro;</strong></td>
      </tr>
    </table>
    <br><br><br><br><br>
    <input class="button" type="button" value="Ricalcolo"><br><br>
    <input class="button" type="button" value="Procedi al passo successivo">
  </div>
</div>
<footer>
  <div class="container">
    <div class="narrow">
      <ul>
        <li><a href="../../contatti/contatti.php">Contatti</a></li>
        <li><a href="../../legal-information/FAQ/FAQ.php">FAQ</a></li>
        <li>
          Iscrizione newsletter:<br>
          <input class="newsletter-input" type="email" name="newsletter-input" placeholder="Indirizzo e-mail">
          <input class="newsletter-submit" type="button" name="newsletter-submit" value="Inviare"/>
        </li>
        <li>
          Modalità di pagamento:
          <ul class="payment-methods">
            <li><img class="payment-method" src="../../organisms/footer/images/mastercard.png" alt="Modalità di pagamento- MasterCard"></li>
            <li><img class="payment-method" src="../../organisms/footer/images/visa.png" alt="Modalità di pagamento- Visa"></li>
            <li><img class="payment-method" src="../../organisms/footer/images/americanexpress.png" alt="Modalità di pagamento- AmericanExpress"></li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="narrow">
      <ul>
        <li><a href="../../legal-information/tariffe-spedizioni/tariffe-spedizioni.php">Tariffe spedizioni</a></li>
        <li><a href="../../legal-information/condizioni-generali-di-vendita/condizioni-generali-di-vendita.php">Condizioni generali di vendita</a></li>
        <li><a href="../../legal-information/note-legali/note-legali.php">Note legali</a></li>
        <li><a href="../../legal-information/cookies/cookies.php">Cookies</a></li>
      </ul>
    </div>
    <div class="narrow">
      <ul>
        <li>
          Chi siamo
          <ul>
            <li><a href="../../chi-siamo/la-famiglia/la-famiglia.php">La famiglia</a></li>
            <li><a href="../../chi-siamo/i-nostri-vini/i-nostri-vini.php">I nostri vini</a></li>
            <li><a href="../../chi-siamo/premi-e-riconoscimenti/premi-e-riconoscimenti.php">Premi e riconoscimenti</a></li>
            <li><a href="../../chi-siamo/il-territorio/il-territorio.php">Il territorio</a></li>
          </ul>
        </li>
        <li>
          Prodotti
          <ul>
            <li><a href="http://www.google.com">Crémant d'Alsace</a></li>
            <li><a href="http://www.google.com">Alsace Tradition</a></li>
            <li><a href="http://www.google.com">Alsace Lieux-Dits</a></li>
            <li><a href="http://www.google.com">Alsace Grands Crus</a></li>
            <li><a href="http://www.google.com">Alsace Pinot Noir</a></li>
            <li><a href="http://www.google.com">Cuvée Prestige</a></li>
            <li><a href="http://www.google.com">Vendages Tardives et Sélections de Grains Nobles</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</footer>
</body>
</html>
