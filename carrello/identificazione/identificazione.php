<<<<<<< HEAD
=======
<?php
/* SCRIPT PHP NECESSAIREMENT EN TETE DE TOUTES LES PAGES WEB.
  le session_start() doit impérativement être la première instruction PHP en haut de page.
*/
session_start();
?>
<?php include '../../organisms/header/header.php';?>
<div class="identificazione-page">
<?php include '../../organisms/header/header.php';?>
<!DOCTYPE HTML>
>>>>>>> origin/maral
<html>
  <head>
    <meta charset="utf-8">
    <title>Koehly produttore e venditore online di vini di Alsazia</title>
    <link rel="stylesheet" type="text/css" href= "../../style.css" />
    <link href="https://fonts.googleapis.com/css?family=Arbutus+Slab" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="/main.js"></script>
  </head>
  <body>
    <header>
      <div class="shopping-bar">
        <div class="container">
          <?php include '../../organisms/header/shopping-bar/shopping-bar-niv-2.php' ?>
        </div>
      </div>
      <div class="navigation">
        <div class="container">
          <?php include '../../organisms/header/header-niv-2.php' ?>
          </ul>
        </div>
    </div>
    <div class="container">
      <a href="../../index.php"><img class="logo-image" src="../../organisms/header/images/logo.svg" alt="" /></a>
    </div>
  </header>
  <div class="identificazione-page">
  <div class="main container">
    <input class="go-back" type="button" onclick="location.href='javascript:history.go(-1)';" value="&#8592; Tornare indietro" />
    <p>1. Il mio carrello/ <strong>2. Identificazione</strong> / 3. Conferma	/ 4. Pagamento</p>
    <h1>2. Identificazione</h1>
    <div class="half">
      <h2>Hai già un account</h2>
      <div class="container">
        <p>Hai già effettuato un ordine passato?</p>
        <p>Inserisci la tua e-mail e password:</p>
        <input type="text" name="email" placeholder="E-mail">
        <input type="text" name="password" placeholder="Password">
        <input class="button" type="button" value="Accedi">
      </div>
    </div>
    <div class="half last">
      <h2>Sei un nuovo utente</h2>
      <div class="container">
        <p>Crea il tuo profilo per completare l'ordine.</p>
        <p>Inserisci la tua e-mail e password:</p>
        <input type="text" name="email" placeholder="E-mail">
        <input type="text" name="password" placeholder="Password">
        <input class="button" type="button" value="Registrati">
      </div>
    </div>
    <br><br><br><br><br><br><br><br><br><br>
    <div class="full">
      <h2>Accedi come visitatore</h2>
      <div class="container">
        <input class="button" type="button" value="Procedi al pagamento">
      </div>
    </div>

  </div>
</div>
<footer>
  <div class="container">
    <div class="narrow">
      <ul>
        <li><a href="../../contatti/contatti.php">Contatti</a></li>
        <li><a href="../../legal-information/FAQ/FAQ.php">FAQ</a></li>
        <li>
          Iscrizione newsletter:<br>
          <input class="newsletter-input" type="email" name="newsletter-input" placeholder="Indirizzo e-mail">
          <input class="newsletter-submit" type="button" name="newsletter-submit" value="Inviare"/>
        </li>
        <li>
          Modalità di pagamento:
          <ul class="payment-methods">
            <li><img class="payment-method" src="../../organisms/footer/images/mastercard.png" alt="Modalità di pagamento- MasterCard"></li>
            <li><img class="payment-method" src="../../organisms/footer/images/visa.png" alt="Modalità di pagamento- Visa"></li>
            <li><img class="payment-method" src="../../organisms/footer/images/americanexpress.png" alt="Modalità di pagamento- AmericanExpress"></li>
          </ul>
        </li>
      </ul>
    </div>
    <div class="narrow">
      <ul>
        <li><a href="../../legal-information/tariffe-spedizioni/tariffe-spedizioni.php">Tariffe spedizioni</a></li>
        <li><a href="../../legal-information/condizioni-generali-di-vendita/condizioni-generali-di-vendita.php">Condizioni generali di vendita</a></li>
        <li><a href="../../legal-information/note-legali/note-legali.php">Note legali</a></li>
        <li><a href="../../legal-information/cookies/cookies.php">Cookies</a></li>
      </ul>
    </div>
    <div class="narrow">
      <ul>
        <li>
          Chi siamo
          <ul>
            <li><a href="../../chi-siamo/la-famiglia/la-famiglia.php">La famiglia</a></li>
            <li><a href="../../chi-siamo/i-nostri-vini/i-nostri-vini.php">I nostri vini</a></li>
            <li><a href="../../chi-siamo/premi-e-riconoscimenti/premi-e-riconoscimenti.php">Premi e riconoscimenti</a></li>
            <li><a href="../../chi-siamo/il-territorio/il-territorio.php">Il territorio</a></li>
          </ul>
        </li>
        <li>
          Prodotti
          <ul>
            <li><a href="http://www.google.com">Crémant d'Alsace</a></li>
            <li><a href="http://www.google.com">Alsace Tradition</a></li>
            <li><a href="http://www.google.com">Alsace Lieux-Dits</a></li>
            <li><a href="http://www.google.com">Alsace Grands Crus</a></li>
            <li><a href="http://www.google.com">Alsace Pinot Noir</a></li>
            <li><a href="http://www.google.com">Cuvée Prestige</a></li>
            <li><a href="http://www.google.com">Vendages Tardives et Sélections de Grains Nobles</a></li>
          </ul>
        </li>
      </ul>
    </div>
  </div>
</footer>
</body>
</html>
