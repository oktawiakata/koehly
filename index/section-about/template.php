<div class="about-section">
  <div class="fullwidth-image">
  </div>
  <div class="container ">
    <h2>Dalla terra e dalla passione produciamo vini sinceri e delicati d'Alsazia.</h2>
    <div class="narrow">
      <div class="about-image">
        <img src="index/section-about/images/Categoria_famiglia_koehly.jpg" alt="" />
      </div>
      <h3>
        La famiglia
      </h3>
      <input class="button" type="button" value="Scoprire di più">
    </div>
    <div class="narrow">
      <div class="about-image">
        <img src="index/section-about/images/Categoria_inostrivini.jpg" alt="" />
      </div>
      <h3>
        I nostri vini
      </h3>
      <input class="button" type="button" value="Scoprire di più">
    </div>
    <div class="narrow">
      <div class="about-image">
        <img src="index/section-about/images/Categoria_territorio_koehly.jpg" alt="" />
      </div>
      <h3>
        Il territorio
      </h3>
      <input class="button" type="button" value="Scoprire di più">
    </div>
  </div>

</div>
