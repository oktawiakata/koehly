<div class="container statistics-section">
  <h2>
    La vigna ed il vino, il nostro mestiere, la nostra passione.<br>
    Jean-Marie Koehly
  </h2>
  <div class="narrow">
    <p>
      7
    </p>
    <p>
      vitigni d'Alsazia
    </p>
  </div>
  <div class="narrow">
    <p>
      50
    </p>
    <p>
      anni di esperienza nella viticoltura
    </p>
  </div>
  <div class="narrow">
    <p>
      23
    </p>
    <p>
      ettari di campagne
    </p>
  </div>
</div>
