<div class="container bestsellers-section">
  <h2>
    Le nostre migliori vendite
  </h2>
  <figure class="narrow">
    <img class="bestsellers-image" src="index/section-bestsellers/images/Cremant_d_Alsace_BLANC_DE_NOIRS_2013.jpg" alt="" />
    <figcaption>
      <h3>Crémant St-Urbain rosé</h3>
      <h4>8.70 &euro;</h4>
    </figcaption>
    <input class="button" type="button" value="Più informazioni">
    <input class="button" type="button" value="Aggiungere">
  </figure>
  <figure class="narrow">
    <img class="bestsellers-image" src="index/section-bestsellers/images/Cremant_d_Alsace_ST_URBAINE_2013.jpg" alt="" />
    <figcaption>
      <h3>Crémant St-Urbain brut Riesling</h3>
      <h4>7.70 &euro;</h4>
    </figcaption>
    <input class="button" type="button" value="Più informazioni">
    <input class="button" type="button" value="Aggiungere">
  </figure>
  <figure class="narrow">
    <img class="bestsellers-image" src="index/section-bestsellers/images/Cremant_d_Alsace_ST_URBAINE_ROSE_2013.jpg" alt="" />
    <figcaption>
      <h3>Crémant Blanc de Noir brut</h3>
      <h4>8.50 &euro;</h4>
    </figcaption>
    <input class="button" type="button" value="Più informazioni">
    <input class="button" type="button" value="Aggiungere">
  </figure>
</div>
