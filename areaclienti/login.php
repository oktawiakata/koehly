<?php
  session_start();
  if(isset($_SESSION['client'])){
      header('Location: dashboard.php');
  }
  require_once '../functions/bdd.php';
  require_once '../functions/client.php';
  $bdd = bdd();
  if(!empty($_POST)){
    $erreur = connexion();
  }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Area Clienti | Login</title>
    </head>
    <body>
      <h1>Area clienti - Login</h2>
        <?php
          if(isset($erreur)):
        ?>
          <p><?= $erreur ?></p>
        <?php
          endif;
        ?>
        <form method="post" action="">
           <label for="email">Email</label>
           <input id="email" name="email" placeholder="Indirizzo mail" value="" type="text" value="<?php if(isset($_POST["email"])) echo $_POST["email"]; ?>">
           <label for="password">Password</label>
           <input id="password" name="password" type="password">
           <a href="#">Ho dimenticato la mia password.</a>
           <input type="submit" value="Connettermi">
        </form>
    </body>

</html>
