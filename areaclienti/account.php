<?php
  session_start();
  if(!isset($_SESSION['client'])){
    header('Location: login.php');
  }
  require_once '../functions/bdd.php';
  require_once '../functions/client.php';
  $bdd = bdd();
  $infos = infos_client();
  if(!empty($_POST))
      $erreurs = modifier_infos_client();
?>
<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>Area clienti | account</title>


  </head>
  <body>
    <nav>
      <ul>
        <li><a href="account.php">Account</a></li>
        <li><a href="ordini.php">Storico del'ordine</a></li>
        <li><a href="signout.php">Sign out</a></li>
      </ul>
    </nav>
      <h1>Benvenuto <?=  $infos["prenom_client"] ?></h1>
      <p>La pagina per modificare le tue informazioni :</h1>
        <?php
          if(isset($erreurs)) :
          if($erreurs) :
          foreach($erreurs as $erreur) :
          ?>
          <p style="color:red"><?= $erreur ?></p>
          <?php
          endforeach;
          else :
          ?>
          <p style="color:green">Vos informations ont été mises à jour!</p>

          <?php
          endif;
          endif;
          ?>
      <form method="post" action="">
        <h2>Informazioni personali</h2>
        <div>
          <label for="nom">Cognome</label>
          <input id="nom" name="nom" type="text" value="<?= $infos["nom_client"] ?>">
        </div>
        <div>
          <label for="prenom">Nome</label>
          <input id="prenom" name="prenom" type="text" value="<?= $infos["prenom_client"] ?>">
        </div>
        <div>
          <label for="mail">Indirizzo mail</label>
          <input id="mail" name="mail" type="text" value="<?= $infos["mail_client"] ?>">
        </div>
        <div>
          <label for="tel">Telefono</label>
          <input id="tel" name="tel" type="text" value="<?= $infos["tel_client"] ?>">
        </div>
        <h2>Indirizzo di consegna</h2>
        <div>
          <label for="rue">Via</label>
          <input id="rue" name="rue" type="text" value="<?= $infos["adresse_client"] ?>">
        </div>
        <div>
          <label for="cp">Codice Postale</label>
          <input id="cp" name="cp" type="text" value="<?= $infos["cp_client"] ?>">
        </div>
        <div>
          <label for="ville">Città</label>
          <input id="ville" name="ville" type="text" value="<?= $infos["ville_client"] ?>">
        </div>

        <h2>Coordinate Bancarie</h2>
        <!--
          I suoi dati personali sono protetti. Garantiamo la protezione di questi dati con un codice criptato dei dati bancari.
          Peut pas affirmer cela car selon M. Chouali, ce n'est pas conforme à la CNIL.
        -->
        <div>
          <input type="radio" name="card" value="visa" <?php if($infos['type_cb']=='visa') echo 'checked' ?>>Visa <input type="radio" name="card" value="mastercard" <?php if($infos['type_cb']=='mastercard') echo 'checked' ?>>Master Card <input type="radio" name="card" value="americanexpress" <?php if($infos['type_cb']=='americanexpress') echo 'checked' ?>>American Express
        </div>
        <div>
          <label for="titulaire_cb">Nome del referento</label>
          <input id="titulaire_cb" name="titulaire_cb" type="text" value="<?= $infos["titulaire_cb"] ?>">
        </div>
        <div>
          <label for="numero_cb">Numero carta di credito</label>
          <input id="numero_cb" name="numero_cb" type="text" value="<?= $infos["num_cb"] ?>">
        </div>
        <div>
          <label>Valida fino a</label>
          <!-- de type MM/AAAA -->
          <!-- mois expiration  -->
          <input name="mois_cb" type="text" value="<?= $infos["mois_valid"] ?>">
          <!-- année expiration -->
          <input name="annee_cb" type="text" value="<?= $infos["an_valid"] ?>">
        </div>
        <div>
          <label for="secu_cb">Codice di sicurezza</label>
          <!-- de type NNN -->
          <input id="secu_cb" name="secu_cb" type="text" value="<?= $infos["num_secu"] ?>">
        </div>
        <div>
          <label>Newsletter</label>
          <input type="radio" name="newsletter" value = "0" <?php if($infos['newsletter']==0) echo 'checked' ?>>No <input type="radio" name="newsletter" value="1" <?php if($infos['newsletter']==1) echo 'checked' ?>>Si
        </div>

        <input type="submit" value="Aggiornare le mie informazioni">
      </form>

  </body>
</html>
