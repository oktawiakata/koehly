<?php
  session_start();
  if(!isset($_SESSION['client'])){
    header('Location: login.php');
  }
  require_once '../functions/bdd.php';
  require_once '../functions/client.php';
  $bdd = bdd();
  $infos = infos_client();
  $commande = infos_commande();

?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <title>Area clienti - Home</title>
  </head>
  <body>
      <nav>
        <ul>
          <li><a href="account.php">Account</a></li>
          <li><a href="ordini.php">Storico del'ordine</a></li>
          <li><a href="signout.php">Sign out</a></li>
        </ul>
      </nav>

      <h1>Benvenuto <?=  $infos["prenom_client"] ?></h1>
      <p>Il suo indirizzo mail : <?= $infos["mail_client"] ?></h1>

  </body>
</html>
