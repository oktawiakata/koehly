<?php
  session_start();
  require_once '../functions/bdd.php';
  require_once '../functions/client.php';
  $bdd = bdd();
  if(isset($_SESSION['client'])){
    header('Location: dashboard.php');
  }

  if(!empty($_POST))
    $erreurs = inscription();

?>
<!DOCTYPE html>
<body>
    <head>
        <meta charset="utf-8">
        <title>Iscrizione</title>
    </head>
    <!-- Style de base qui peut être reprit par Oktawia -->
    <style>
        label, input {
            display: block;
            margin: 5px 0 5px 0;
        }
    </style>
    <body>
      <h1>Iscrizione</h1>
      <?php
            if(isset($erreurs)) :
            if($erreurs) :
            foreach($erreurs as $erreur) :
            ?>
            <p><?= $erreur ?></p>
            <?php
            endforeach;
            else :
            ?>
            <p>Votre inscription a bien été prise en compte !</p>
            <p><a href="login.php" title="Login">Cliquez ici pour vous connecter</a></p>
            <?php
            endif;
            endif;
            ?>
       <form method="post" action="">
           <div>
            <label for="nom">Cognome</label>
            <input id="nom" name="nom" type="text" placeholder="Cognome" value="<?php if(isset($_POST['nom'])) echo $_POST['nom'] ?>">
        </div>
        <div>
            <label for="prenom">Nome</label>
            <input id="prenom" name="prenom" type="text" placeholder="Nome" value="<?php if(isset($_POST['prenom'])) echo $_POST['prenom'] ?>">
        </div>
          
        <div>
            <label for="email">Indirizzo mail</label>
            <input id="email" name="email" type="text" placeholder="Email" value="<?php if(isset($_POST['email'])) echo $_POST['email'] ?>">
        </div>
        <div>
            <label for="password">Password</label>
            <input id="password" name="password" type="password">
        </div>
        <div>
            <label for="password-conf">Conferma password</label>
            <input id="password-conf" name="password-conf" type="password">
        </div>
        <input type="submit" value="Crea account">
       </form>
        
    </body>
</body>