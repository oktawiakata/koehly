<?php
  session_start();
  if(!isset($_SESSION['client'])){
    header('Location: login.php');
  }
  require_once '../functions/bdd.php';
  require_once '../functions/client.php';
  $bdd = bdd();
  $infos = infos_client();
  $commandes = infos_commande();

?>
<!DOCTYPE html>
<html lang="it">
  <head>
    <meta charset="utf-8">
    <title>Dettaglio del'ordine</title>
  </head>
  <body>
    <nav>
      <ul>
        <li><a href="account.php">Account</a></li>
        <li><a href="ordini.php">Storico del'ordine</a></li>
        <li><a href="signout.php">Sign out</a></li>
      </ul>
    </nav>
      <h1>Benvenuto <?=  $infos["prenom_client"] ?></h1>
      <p>Il suo indirizzo mail : <?= $infos["mail_client"] ?></h1>
      <table>
          <thead>
              <tr>
                <td>Numéro de commande</td>
                <td>Date</td>
                <td>Moyen de paiement</td>
                <td>Etat paiement</td>
                <td>Etat commande</td>
                <td>Prix commande</td>
                <td>Frais de livraison</td>
              </tr>
          </thead>
          <tbody>
          <?php
            foreach ($commandes as $commande):
          ?>
            <tr>
              <td><?= $commande["num_commande"] ?></td>
              <td><?= $commande["date_commande"] ?></td>
              <td><?= $commande["moyen_paiement"] ?></td>
              <td><?= $commande["etat_paiement"] ?></td>
              <td><?= $commande["etat_commande"] ?></td>
              <td><?= $commande["prix_commande"] ?> €</td>
              <td><?= $commande["frais_livraison"] ?> €</td>
           </tr>
        <?php
          endforeach;
        ?>
        </tbody>
    </table>
  </body>
</html>
